{
    "pageDirectory": "pages/",
    "intro": false,
    "introURL": "../intro.html",
    "tierDisplay": [["course", "module"], ["lesson", "page"]],
    "randomizeAssessment": false,
    "assessmentPooling": false,
    "numAssessmentQuestions": 2,
    "bookmarking": true,
    "bookmarkingMsg": "Welcome back <span class='studentName'></span>! Would you like to resume the course from your previous location, or start from the beginning?",
    "useMenuDescriptions": false,
    "pageAcross": true,
    "courseLocked": false,
    "forceSequential": false,
    "lockAssessment": false,
    "useTracking": false,
    "trackingAPI": "SCORM",
    "scormVersion": "1.2",
    "interactionTrackingType": "stateful",
    "hasAssessment": false,
    "letteredDistractors": false,
    "secureAssessment": false,
    "masteryScore": 80,
    "submitBtn": "Submit",
    "volume": 80,
    "jumpMenu": true,
    "skipMenu": true,
    "sidebar": true,
    "certificate": true,
    "sidebarHandle": ["chevron-right", "chevron-left"],
    "topNavigation": [
        {
            "id": "help",
            "name": "HELP",
            "icon": "info-circle",
            "top": "top"
        },
        {
            "id": "exit",
            "name": "EXIT",
            "icon": "sign-out",
            "top": "top"
        }
    ],
    "navigation": {
        "right": [
            {
                "id": "replay",
                "name": "REPLAY",
                "icon": "refresh",
                "type": "main"
            },
            {
                "id": "pause",
                "name": "PAUSE",
                "icon": "pause",
                "toggle": {
                    "name": "PLAY",
                    "icon": "play"
                },
                "type": "main"
            },
            {
                "id": "back",
                "name": "BACK",
                "icon": "arrow-circle-left",
                "type": "main"
            },
            {
                "id": "next",
                "name": "NEXT",
                "icon": "arrow-circle-right",
                "type": "main",
                "swap": true
            }
        ],
        "left": [
            {
                "id": "captions",
                "name": "CAPTIONS ON",
                "icon": "commenting",
                "toggle": {
                    "name": "CAPTIONS OFF",
                    "icon": "comment"
                }
            },
            {
                "id": "resources",
                "name": "RESOURCES",
                "icon": "folder-open"
            }
        ]
    },
    "resources": [
        {
            "name": "Beatitudes Employee Portal",
            "url": "http://bccrew.net/",
            "icon": "external-link"
        },
        {
            "name": "Beatitudes Campus Website",
            "url": "http://www.beatitudescampus.org/",
            "icon": "external-link"
        },
        {
            "name": "Human Resources (HR) Staff",
            "url": "resources/hr-org-chart.pdf",
            "icon": "file-pdf-o"
        }
    ],
    "glossary": [
        {
            "id": "Term One ",
            "name": "Term One ",
            "definition": "Sample Glossary Term"
        },
        {
            "id": "Term Two ",
            "name": "Term Two ",
            "definition": "Sample Glossary Term"
        },
        {
            "id": "Term Three ",
            "name": "Term Three ",
            "definition": "Sample Glossary Term"
        }         
    ],
    "help": {
        "left": [
            {
                "text": "Select the NEXT and BACK buttons to navigate through the course. The PAUSE button stops the page you are currently viewing and the PLAY button restarts the page from the point that it was paused. The REPLAY button repeats the current page.",
                "buttons": ["replay", "pause", "back", "next"]
            },
            {
                "text": "Select the captions (CAPTIONS ON) button to display the text of the audio narration. Clicking captions (CAPTIONS OFF) button again hides the captioning text.",
                "buttons": ["captions"]
            }
        ],
        "right": [
            {
                "text": "Select the RESOURCES button to display a popup with links for accessing additional information regarding the content of the module. Closing the information returns to the module.",
                "buttons": ["resources"]
            },
            {
                "text": "Select the HELP button to display this help popup. Select the close (x) button to closes this popup.",
                "buttons": ["help"]
            },
            {
                "text": "Select the EXIT button to leave the course at any time.",
                "buttons": ["exit"]
            }
        ]
    }
}