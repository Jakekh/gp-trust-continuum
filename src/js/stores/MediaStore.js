import { EventEmitter } from "events"

import dispatcher from "../dispatcher"

class MediaStore extends EventEmitter {
	constructor() {
		super()
	}

	stopMedia(componentId) {
		this.activeMedia = componentId
		//console.log("Stoping all Players")
		this.emit("stop_media")
	}
	handleActions(action) {
		switch(action.type) {
			case "TEMP_ACTION": {
				this.tempHandler(action.prop)
				break
			}
		}
	}
}

const mediaStore = new MediaStore

dispatcher.register(mediaStore.handleActions.bind(mediaStore))

export default mediaStore