import { EventEmitter } from "events"

import dispatcher from "../dispatcher"

class MsgStore extends EventEmitter {
	constructor() {
		super()
		this.msg = {title: ""}
	}

	createMsg(message) {
		this.msg = message
		this.emit("change")
	}

	getMessage() {
		return this.msg
	}

	handleActions(action) {
		switch(action.type) {
			case "CREATE_MESSAGE": {
				this.createMsg(action.msg)
				break
			}
		}
	}
}

const msgStore = new MsgStore

dispatcher.register(msgStore.handleActions.bind(msgStore))

export default msgStore