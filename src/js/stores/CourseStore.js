import { EventEmitter } from "events"

import $ from 'jquery'
import dispatcher from "../dispatcher"

import LmsStore from './LmsStore'

class CourseStore extends EventEmitter {
	constructor(){
		super()
		let themeFile = require('../themes/defaultTheme.js')
		this.courseData = require('../../data/course-data.json')
		this.glossaryData = require('../../data/glossary.json')
		this.splitThresholds(this.courseData)
		this.theme = themeFile.default()
	}
	splitThresholds(data){
		/*Map through all modules*/
		this.moduleThresholds = []
		let that = this
		 _.map(this.courseData.modules, function(module, modkey){
			let sectionThresholds = []
			let tmpAry = []
			let lessonAry = []

			if(module.lessons){
				_.map(module.lessons, function(lesson, lesKey){
					lessonAry = []
					////console.log("Lesson: ", lesson.title, "# Sections: ", lesson.sections.length)
					_.map(lesson.sections, function(section, seckey){
					 	let isThreshold = _.find(section, function(o) { return o.threshold  })
						if(isThreshold ){
							////console.log("HAVE THRESHOLD")
							tmpAry.push(section)
							lessonAry.push(tmpAry)
							tmpAry = []
						}else{
							tmpAry.push(section)
						}
					})
					sectionThresholds.push(lessonAry)
				})
			}else{
				/*For each module, map through all sections  - CLEAN THIS UP*/
				 _.map(module.sections, function(section, seckey){
				 	let isThreshold = _.find(section, function(o) { return o.threshold  })
					if(isThreshold ){
						tmpAry.push(section)
						sectionThresholds.push(tmpAry)
						tmpAry = []
					}else{
						tmpAry.push(section)
					}
				})
			}
			
			that.moduleThresholds.push(sectionThresholds)
			////console.log("THRESHOLDS: ", that.moduleThresholds)
		})
	}
	addThreshold(){
		//////console.log('addThreshold: ', this, LmsStore.progress[this.location[0]][this.location[1]])
		let nextThreshold = this.moduleThresholds[this.location[0]][this.location[1]][this.location[2]]
		const thresholdComplete = LmsStore.progress[this.location[0]][this.location[1]][this.location[2]]
		////console.log( nextThreshold, thresholdComplete, this.location )
		if(nextThreshold){
			this.view = this.view.concat(nextThreshold)
			LmsStore.setProgress(this.location)
			////console.log("RETURNING TH:")
			this.emit("view_changed")
		}
	}
	loadModule(newLocation){
		//////console.log("PROGRESS:", LmsStore.getProgress())
		if(newLocation){
			//////console.log('newLocation: ', newLocation)
			this.location = newLocation
			LmsStore.setLocation(newLocation)
		}
		if(this.location){
			////console.log('location exists, location is: ', this.location)
			const that = this
			const progress = LmsStore.getProgress()[this.location[0]]
			////console.log(progress, LmsStore.getProgress())
			if(progress[this.location[1]].indexOf(1) != -1) {
				this.view = []
				////console.log('progress.includes(1): ', progress.includes(1))
				_.map(progress[this.location[1]], (value, key) => {
					value == 1 ? that.view = that.view.concat(this.moduleThresholds[this.location[0]][this.location[1]][key]) : null
				})
			}else {
				//console.log(this.moduleThresholds)
				this.view = this.moduleThresholds[this.location[0]][this.location[1]][this.location[2]]
			}
		}else{
			this.location = [0,0,0]
			LmsStore.setLocation([0,0,0])
			this.view = this.moduleThresholds[this.location[0]][this.location[1]][this.location[2]]
		}
		//////console.log("NEW VIEW: ", this.view)
	}
	returnCourseData(){
		/*Return entire course object*/
		return this.courseData
	}
	returnCourseView(){
		/*Return only current view data - based on progress*/
		return this.view
	}
	returnLocation(){
		/*Return only current view data - based on progress*/
		return this.location
	}
	returnThresholds() {
		return this.moduleThresholds
	}

    closeWindow() {
    	let win = window
        while (win != win.parent)
        win = win.parent
        win.close()
    }

	showResources = () => {
		this.emit('show_resources')
	}

	showGlossary = () => {
		this.emit('show_glossary')
	}

	returnDisplayWidth = () => {
		return $(window).width()
	}
	generateQuestions = ( pools ) => {
		let that = this
		this.questions = []

		_.map(pools, function(pool, key){
			_.shuffle(pool.questions)
			let poolSample = _.sampleSize(pool.questions, pool.pull )
			that.questions.push(poolSample)
		})

		this.questions = _.flattenDeep(this.questions)
		////console.log("Questions Created", this.questions)
		this.emit('questions_generated')
	}
	returnQuestions = () => {
		return this.questions
	}
	toggleMenu = () => {
		this.menuOpen = !this.menuOpen
		this.emit("menuToggle")
		$("#contentHolder").css({transition: "paddingLeft, 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms",
				paddingLeft: this.menuOpen && window.innerWidth > 840 ? "300px" : "0px"})
		$("#assetBank").css({transition: "paddingLeft, 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms",
				paddingLeft: this.menuOpen && window.innerWidth > 840 ? "300px" : "0px"})
	}
	handleActions(action) {
		switch(action.type) {
			case "UPDATE_PROGRESS" : {
				LmsStore.setProgress(action.location)
				break
			}
			case "GETTING_VIEW": {
				LmsStore.setProgress(action.location)
				this.loadModule(action.location)
				this.emit("view_changed")
				break
			}
			case "LOAD_NEW_MODULE": {
				this.loadModule([action.module, action.lesson, 0])
				LmsStore.setProgress(this.location)
				$(window).scrollTop(0)
				this.emit("view_changed")
				break
			}
			case "NEXT_THRESHOLD": {
				let module = this.location[0]
				let lesson  = this.location[1]
				let section  = this.location[2]
				if(this.moduleThresholds[module][lesson][section+1]){
					////console.log("GETTING TH: STORE")
					this.location[2] = this.location[2]+1
					this.addThreshold()
					this.emit("view_changed")
				}else{
					////console.log("All Thresholds loaded...")
				}
				break
			}
			case "SHOW_GLOSSARY_TERM": {
				this.infolinkTerm = _.find(this.glossaryData, (item) => {
					//console.log(item.term, action.term)
					return item.term == action.term
				})
				//console.log(this.infolinkTerm)
				this.emit("show_infolink")
				break
			}
			case "SAVE_ASSESSMENT_REM": {
				this.assessmentRemediation = action.rem
				this.emit("assessment_review_change")
				break
			}
		}
	}
}

const courseStore = new CourseStore

dispatcher.register(courseStore.handleActions.bind(courseStore))

export default courseStore