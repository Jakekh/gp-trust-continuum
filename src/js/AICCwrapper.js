/*Tracorp AICC Wrapper v1.0*/

import $ from 'jquery'
import MsgStore from './stores/MsgStore'
import LmsStore from './stores/LmsStore'
import CourseStore from './stores/CourseStore'

class AICC {
    constructor() {
        this.aiccObj = {}
        this.AICCDebug = false
    }

    setAICC(progress, cond) {
        this.init({
            action:'set',
            score: LmsStore.getScore(),
            time: LmsStore.setSessionTime(),
            credit: 'c',
            location: LmsStore.returnLocation(),
            status: LmsStore.getStatus(),
            progress: progress,
            callback: () => { cond == 'exit' ? CourseStore.closeWindow(window) : null }
        })
    }

    getAICC(  callback ) {
        this.init({ action:'get', callback: callback })
    } 

    getUrlVar(urlVar) {   
        var match = RegExp(`[?&]${urlVar}=([^&]*)`).exec(window.location.search)
        return unescape(match && decodeURIComponent(match[1].replace(/\+/g, ' ')))
    }

    init(obj) {
        if(this.AICCDebug){
            MsgStore.createMsg({title: "AICC Debug", message: "Please make a selection.", type: 'popup', customActions: [{label: "Send AICC", action: this._set.bind(this, obj)}, {label: "Get AICC", action: this.getAICC}, {label: "Close", action: null}]})
        }
        this._sid = this.getUrlVar('aicc_sid')
        this._url = this.getUrlVar('aicc_url')
        obj.action == 'set' ? this._set(obj) : this._get(obj)
    }

    _set({ score, time, credit, location, status, progress, callback }) {
        //replace all ',' with '-' at send time in progress and lesson location
        const locationString = location.toString().replace(/,/g, "-")
        const progressString = progress.replace(/,/g, "-")

        //build url
        const dataURL = `command=PutParam\n&version=2.2\n&session_id=${this._sid}\n&aicc_data=[core]\ncredit=${credit}\nlesson_location=${locationString}\nlesson_status=${status}\nscore=${score}\ntime=${time}\n[core_lesson]\nprogress=${progressString}`
        
        const jqxhr = $.ajax({
            method: "POST",
            url: this._url,
            data: dataURL,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        })
        .done( (data) => {
            if(this.AICCDebug){
                console.log('Succcess...')
                console.log('AICC DATA: ', dataURL)
            }
            console.log('success')
            //callback()
        })
        .fail( (xhr, status, err) => {
            if(this.AICCDebug){
                console.log('Error sending data...')
            }
            alert(`There was an error saving progress. Please relaunch the course. (Error code: ${xhr.status})`)
            callback()
        })
    }

    _get({callback}) {
        const dataURL = `${this._url}?command=getParam&version=3.0&session_id=${this._sid}`

        const jqxhr = $.ajax( {
            method: "POST",
            url: dataURL,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        })
        .done( (xhr) => {
            if(this.AICCDebug){
                console.log('Succcess...')
            }
            const valuesAry = xhr.split('\n')

            //split by '=', and merge into aiccObj
            _.map(valuesAry, (value, key) => {// -- kvp = key-value-pair --
                const kvp = valuesAry[key].split('=')
                if(kvp[1] != ''){
                    if(kvp[0] == 'lesson_location' || kvp[0] == 'progress'){
                        kvp[1] = kvp[1].replace(/-/g,',')
                    }
                    this.aiccObj[kvp[0]] = kvp[1]
                }else{
                    this.aiccObj[kvp[0]] = ''
                }
            })

            callback( this.aiccObj )
        })
        .fail( (xhr, status, err) => {
            if(this.AICCDebug){
                console.log('Error retrieving data...')
            }
            if(xhr.status == 0){
                alert('Progress could not be loaded for this course. The operation was blocked by the server.')
            }else{
                alert(`There was an error: (Error :${xhr.status})`)
            } 
        })
    }
}

const aicc = new AICC()

export default aicc

//     if (this.aiccObj.student_name.indexOf(",") != -1) {
//         var studentNameAry = this.aiccObj.student_name.split(',');
//         studentName = studentNameAry[1] + ' ' + studentNameAry[0];
//     }