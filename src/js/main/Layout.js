import "babel-polyfill";

var React = require('react')

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import defaultTheme from '../themes/defaultTheme'
import { createMuiTheme } from 'material-ui/styles'
import CourseStore from '../stores/CourseStore'

import Message from '../components/messages/message'
import Modal from '../components/UI/Modal'
import Toolbar from '../components/UI/Toolbar'
import Resources from '../components/UI/Resources'
import { CircularProgress } from 'material-ui/Progress'

export default class Layout extends React.Component {
	constructor(){
		super()
		this.state = {
			hasData: CourseStore.returnCourseData() ? true : false,
			course: CourseStore.returnCourseData(),
		}
	}
	componentWillMount = () => {
		this.awaitData()
	}
	awaitData = () => {
		let attempts = 0
		let that = this
		var apiInterval = setInterval(function(){
			attempts++
			if(CourseStore.returnCourseData()){
				clearInterval(apiInterval)
				that.setState({course: CourseStore.returnCourseData(), hasData: true})
			}else{
				//FOR TESTING LET IT FAIL
				if(attempts > 40){
					clearInterval(apiInterval)
					that.setState({course: CourseStore.returnCourseData(), hasData: true})
				}
			}
		}, 200)
	
	}
	render() {
		const styles = {
			loader: {
			  	width: '100%',
			  	textAlign: 'center',
			  	marginTop: "400px",
		  	},
		}

		const theme = createMuiTheme(defaultTheme())
		const {course, hasData} = this.state
		return (

			<MuiThemeProvider theme={ theme } >
				{ hasData ? (
					<div class="mdl-layout">
						{ course ?
							<div >
							   	<div id="contentHolder" style={styles.contentHolder}>
								   	<Toolbar course={ course }/>
									<div class="layoutDiv" >
										{ this.props.children }		
									</div>
								</div>
							</div>
						: 
							<p>Loading</p>
						}
						<Message />
						<Modal />
						<Resources />
					</div>
				) :	(<div style={ styles.loader }><CircularProgress size={ 50 } /></div> ) } 	
			</MuiThemeProvider>
		)	
	}
}
