Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = defaultTheme 

//const _colors = require('material-ui/styles/colors')
//const _colorManipulator = require('material-ui/utils/colorManipulator')

const theme = {
  typography: {
    // Use the system font over Roboto.
    fontFamily: 'Agenda-Medium, Arial, san-serif !important'
  },
  
  palette: {
    primary: {
      light: '#003fb7',
      main: '#002469',
      dark: '#00091b',
      contrastText: '#fff',
    },
    secondary: {
      light: '#f74152',
      main: '#e1091d',
      dark: '#830511',
      contrastText: '#fff',
    }
  }
}

function defaultTheme() {
   return theme
};
