import * as CourseActions from '../actions/CourseActions'
import CourseStore from '../stores/CourseStore'
import React from 'react'
import Banner from "./UI/Banner"
import Section from "./UI/Section"
import Footer from "./UI/Footer"
import SectionDivider from "./UI/SectionDivider"
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'
export default class View extends React.Component {
	constructor(){
		super()
		this.state = {
			view: CourseStore.returnCourseView(),
		}
	}
	componentWillMount() {
		CourseStore.on( "view_changed", this._returnCourseView )
	}
	_returnCourseView = () =>{
		//console.log("VIEW CHANGED, view")
		this.setState({
			view: CourseStore.returnCourseView()
		})
	}
	render() {
		let that = this
		const{ view } = this.state
		const styles = {
			chip: {
				borderRadius: '20px',
			    height: '40px',
			    transition: 'all .5s'
			},
		}
		const components = {
		    Banner: Banner,
		    Section: Section,
		    Footer: Footer,
		}

		const location = CourseStore.returnLocation()
		
		let componentIndex = 0

		let childComponents = _.map( view, function(value, componentKey){
			componentIndex ++
			////console.log(componentKey, view.length, componentIndex, value)
			let subComps =_.map( value, function(component, compKey){
				const ComponentType = components[compKey]
		    		return <ComponentType muiTheme={that.props.muiTheme} { ...component } key={ componentIndex } sectionId={ `${location[0]}_${location[1]}_${componentKey}` }/>
		    	
	    	})
			return subComps
		})
		
		return (
			<div>
			 { childComponents }
			</div>
		)	
	}
}
