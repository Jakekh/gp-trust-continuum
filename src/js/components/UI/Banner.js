var React = require('react')
var videojs = require('video.js')
import ReactHtmlParser from "react-html-parser"
import 'animation.gsap'
import 'debug.addIndicators'
import 'TextPlugin'
var ScrollMagic = require('scrollmagic')
import {TweenMax, TimelineLite, Animation} from "gsap"
import Button from 'material-ui/Button'

import ModuleStats from  './ModuleStats'
import InteractionBumper from './InteractionBumper'

import * as CourseActions from '../../actions/CourseActions'
import CourseStore from '../../stores/CourseStore'
import LmsStore from '../../stores/LmsStore'

export default class Banner extends React.Component {
	constructor(){
		super()

		this.state = {
			videoVisible: false
		}
	}

	start = () => {
		const location = LmsStore.returnLocation()
		const courseData = CourseStore.returnCourseData()
		courseData.modules[location[0]].lessons[location[1]+1] ? CourseActions.loadModule({ module: location[0], lesson: location[1]+1 }) : $("html, body").animate({ scrollTop: 0}, 500, "swing")
	}

	loadVideo = () => {
		this.setState({
			videoVisible: true
		}, function(){
			this.player = videojs(this.videoNode, this.props.video, function onPlayerReady() { });
		})
	}

	componentDidMount() {
		var controller = new ScrollMagic.Controller({loglevel: 0, addIndicators: false})
		if(this.props.parralaxLayers){
			var paralaxAnimPart = new TimelineMax();
			 _.map(this.props.parralaxLayers, function(value, key){
			 	if(value.direction == "from"){
					paralaxAnimPart.from(`#paralax_layer_${key}`, 1, value.animation, 0);
			 	}else{
			 		paralaxAnimPart.to(`#paralax_layer_${key}`, 1, value.animation, 0);
			 	}
			})
			var bg_scene = new ScrollMagic.Scene({
				triggerElement: '#bannerHolder', 
				triggerHook: 0,
				duration: 1000,
			})
		    	.addTo(controller)
		    	.setTween(paralaxAnimPart) 
	    	var bannerContentTween = new TimelineMax();
		    bannerContentTween.to('#bannerContent', 1, {opacity: 0, ease: Power2.easeOut}, 0)
 
			var bannerContent_scene = new ScrollMagic.Scene({
				triggerElement: '#bannerContent', 
				triggerHook: 0,
				duration: 1000,
			})
	    	.addTo(controller)
	    	.setTween(bannerContentTween)
		}	
		     	

	    	
	}

	componentWillUnmount() {
		if (this.player) {
	  		this.player.dispose()
		}
	}

	render() {
		const styles = {
			bannerHolder: {
				backgroundImage: this.props.heroImg ? `url(${this.props.heroImg})` : "none",
				backgroundColor: "#002469",
				backgroundSize: "cover",
			   	backgroundPosition: "center",
			    backgroundRepeat: 'no-repeat',
				display: "flex",
				alignItems: "center",
  				justifyContent: 'center',
  				verticalAlign: "center",
  				minHeight: "100vh",
			},
			bannerContent: {
				position: "relative",
				textAlign: 'center',
			},
			banner: {
				flex: 1,
				verticalAlign: "center",
				padding: "30vh 0",
			},
			bannerTitle: {
				zIndex: 999,
				color: "#fff",
				textShadow: "2px 2px 2px rgba(0,0,0,.3)",
			},
			bannerSubTitle: {
				marginTop: 0,
				color: "#fff",
			},
			bannerSubText: {
				color: "#fff",
			},
			bannerDirections: {
				fontStyle: "italic",
				color: "#fff",
				fontSize: 18,
			},
			videoPoster: {
				border: "1px solid #fff",
				maxWidth: "100%",
			},
			paralaxLayer: {
				width: "100%",
				backgroundSize: "cover",
				position: "absolute",
				overflow: "hidden",
				top: 0,
				left: 0,
				zIndex: -1,
			},
			modStats: {
				paddingTop: 80
			},
			absoluteContainer: {
				position: "absolute",
				top: 0,
				left: 0,
				width: "100%",
				overflow: "hidden",
			}
		}
		const {videoVisible} = this.state
		const sectionName = this.props.title.replace(/\W+/g, '-').toLowerCase()

		//Paralax Layers
		let paralaxLayers = null
		this.props.parralaxLayers ?
			paralaxLayers = _.map(this.props.parralaxLayers, function(value, key){
				return <div id={"paralax_layer_"+key} key={ "paralax_layer_"+ key } style={{ ...styles.paralaxLayer, backgroundImage: `url(${value.src})`}}></div>
			})
		: null

		return (
			<div>
				<div id="bannerHolder" style={styles.bannerHolder}>
					<div id={sectionName} style={ styles.banner } key="banner">
						<div style={styles.absoluteContainer}>
							{ paralaxLayers }
						</div>
						<div class="container" id="bannerContent" style={styles.bannerContent}>
							<div class="mdl-grid">
								<div class="mdl-cell mdl-cell--12-col">
									{this.props.image ? 
										<img src={this.props.image} />
									:null }
									<h1 style={styles.bannerTitle} id="banner_title">{this.props.title} </h1>
									{this.props.subTitle ? <h4 style={styles.bannerSubTitle}>{this.props.subTitle}</h4> : null }
								</div>
								{this.props.subTitle || this.props.text || this.props.directions ?
									<div class={ this.props.video ? "mdl-cell mdl-cell--4-col" : "mdl-cell mdl-cell--12-col" }  >
										{this.props.text ? <p style={styles.bannerSubText}>{this.props.text}</p> : null }
										<Button variant={ "raised" } onTouchTap={ this.start }>Start</Button>
									</div>
								: null }
								{ this.props.video ?
									<div class="mdl-cell mdl-cell--8-col">
										{ videoVisible ? 
											<div data-vjs-player>
												<video ref={ node => this.videoNode = node } className="video-js vjs-fluid"></video>
											</div>
										: 
											<img style={styles.videoPoster} onTouchTap={this.loadVideo} src={this.props.video.poster} /> 
										}
									</div>
								: null }
							</div>
							{this.props.stats ? 
								<div style={styles.modStats}>
									<ModuleStats stats={this.props.stats} />
								</div>
							: null }
						</div>
					</div>
				</div>
				<InteractionBumper style={{background: "#fff", color: "#002469", position: 'absolute', bottom: 0}} icon={ false } text={ this.props.directions } />
			</div>
		)	
	}
}
