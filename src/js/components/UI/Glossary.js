var React = require('react')

import Drawer from 'material-ui/Drawer';
import Menu, { MenuItem } from 'material-ui/Menu';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';
import Divider from 'material-ui/Divider';

import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import Typography from 'material-ui/Typography';
import * as CourseActions from '../../actions/CourseActions'
import CourseStore from '../../stores/CourseStore'
import Toolbar from 'material-ui/Toolbar';
import ReactHtmlParser from "react-html-parser"

const styles = {
  root: {
    width: '100%',
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

class Glossary extends React.Component {
	constructor(){
		super()
		this.state = {
			glossary: require('../../../data/glossary.json'),
			open: false
		}
	}
	componentWillMount() {
		CourseStore.on( "show_glossary", () => {this.setState({open: true })})
		CourseStore.on( "show_infolink", () => {
			this.setState({open: true })
			this._showTerm(CourseStore.infolinkTerm)
		})
	}
	_closeResources = () => {
		this.setState({
			open: false
		})
	}
	_showTerm = (item) => {
		this.setState({
			activeItem: item
		})
	}
	render() {
		let that = this
		const {glossary, open, activeItem} = this.state
		let glossaryEls = _.map(glossary, function(item, index){
			return [<MenuItem onTouchTap={ that._showTerm.bind(this, item) } ><div class="truncate">{item.term}</div></MenuItem>, <Divider />]
		})
		const styles = {
			centerContent: {
				width: "100%",
				textAlign: "center",
				paddingTop: "20px",
			},
			drawer: {
				height: "auto",
				minHeight: "100vh",
				background: "#fff",
				width: "100%",
				maxWidth: "400px",
			},
			toolbar: {
				background: "#fff",
			    boxShadow: "rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px",
			    color: "#ccc",
			    height: "48px",
			},
			toolbarTitle: {
				lineHeight: "48px",
				paddingLeft: "16px",
			},
			active: {
				padding: "16px",
			},
			activeTerm: {
				fontSize: "24px",
				fontWeight: "bold",
				marginBottom: "15px"
			},
			activeDef: {
				paddingLeft: "10px",
				paddingBottom: "20px",
			}
		}
		
		return (
			<Drawer open={open} width={300}>
				<Toolbar>
					<Typography variant="title" color="inherit">Glossary</Typography>
		         	<IconButton onTouchTap={this._closeResources} touch={true}>
		                <Icon className="material-icons">close</Icon>
	              	</IconButton>
			    </Toolbar>
			    { activeItem ? 
			    	<div style={styles.active}>
			    		<div style={styles.activeTerm}>{ReactHtmlParser(activeItem.term)}</div>
			    		<div style={styles.activeDef}>{ReactHtmlParser(activeItem.def)}</div>
			    		<div style={styles.centerContent}>
			    			<IconButton onTouchTap={ () => {this.setState({activeItem: null})} }>
       							<Icon className="material-icons">close</Icon>
    						</IconButton>
   						</div>
			    	</div>
			    :
          			glossaryEls 
          		}
	        </Drawer>
		)	
	}
}

Glossary.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Glossary);