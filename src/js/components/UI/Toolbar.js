var React = require('react')

import ReactHtmlParser from "react-html-parser"

import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'

import IconButton from 'material-ui/IconButton'
import Icon from 'material-ui/Icon'
import Menu, { MenuItem } from 'material-ui/Menu'
import { ListItemIcon, ListItemText } from 'material-ui/List'
import Button from 'material-ui/Button'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'
import Help from './Help'

import LmsStore from '../../stores/LmsStore'
import MsgStore from '../../stores/MsgStore'
import CourseStore from '../../stores/CourseStore'
import AssetStore from '../../stores/AssetStore'
import * as AssetActions from '../../actions/AssetActions'

import { withTheme } from 'material-ui/styles'

class Toolbar_ui extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			menuOpen: CourseStore.menuOpen,
			reviewSummary: null,
		}
	}
	exit = () => {
		MsgStore.createMsg({title: "Exit", message: "Are you sure you want to exit the course?", type: 'prompt', submit: LmsStore.saveSuspendData.bind(this, 'exit')})
		this.setState({anchorEl: null})
	}
	
	help = () => {
		MsgStore.createMsg({title: "Help", message: <Help />, type: 'help', size: 'wider', customActions: [{label: "Close", action: null}]})
		this.setState({anchorEl: null})
	}
	reviewSummary = () => {
		//console.log(CourseStore.assessmentRemediation)
		MsgStore.createMsg({title: "Review Summary", message: CourseStore.assessmentRemediation, type: 'popup-unparsed', size: 'wider', customActions: [{label: "Close", action: null}]})
		this.setState({anchorEl: null})
	}
	search = () => {
		AssetStore.createResultsModal()
		this.setState({anchorEl: null})
	}
	componentDidMount(){
		let that = this
		CourseStore.on("menuToggle", function(){that.setState({ menuOpen: CourseStore.menuOpen })})
	}
	componentWillUnmount(){
		let that = this
		CourseStore.removeListener("menuToggle", function(){that.setState({ menuOpen: CourseStore.menuOpen })})
	}
	handleClick = event => {
    	this.setState({ anchorEl: event.currentTarget })
    	AssetActions.stopSearch()
  	}

 	handleClose = () => {
    	this.setState({ anchorEl: null });
  	}
	updateToolbar = () => {
		this.setState({
			reviewSummary: CourseStore.assessmentRemediation ? CourseStore.assessmentRemediation : null
		})
	}
	toggleMenu = () => {
		CourseStore.toggleMenu()
		AssetActions.stopSearch()
	}
	/*componentWillReceiveProps(nextProps){
		nextProps && this.state.menuOpen != nextProps.menuOpen ?
			this.setState({menuOpen: nextProps.menuOpen })
		: null
	}*/
	render() {
		const {course, classes} = this.props
		const {menuOpen, reviewSummary} = this.state
		const styles = {
			fixedToolbar: {
				position: "fixed",
				top:0,
			    zIndex: "100",
			    width: menuOpen ? "calc(100vw - 300px)" : "100%",
			    transition: "width 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms"
			},
			toolbar: {
				transition: "width 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms",
				background: "#fff",
				height: "48px",
			    boxShadow: "rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px",
			    color: "#fff",
			    zIndex: "99999",
			},
			courseTitle: {
				lineHeight: "48px",
				paddingLeft: "20px",
			},
			logo: {
				height: '100%',
			},
			toolbarMenu: {
				
			},
			toolbarButton: {
				margin: "6px",

			}
		}
		
		/*{ reviewSummary ? 
    		<Button variant={"raised"} style={styles.toolbarButton} onTouchTap={this.reviewSummary.bind(this)}>Review Summary</Button>
    		: null
    	}*/
		
		return (
			<div style={styles.fixedToolbar}>
				<Toolbar style={styles.toolbar}>
				    <Typography variant="title" >{ ReactHtmlParser(course.title) }</Typography>
				    <div style={{flex: 1}} ></div>
		        	<Button color="primary" onTouchTap={ CourseStore.showResources.bind(this) }>
			        	<Icon class="material-icons">insert_link</Icon>&nbsp;&nbsp;Resources
			      	</Button>
			    </Toolbar>
		    </div>
		)	
	}
}

/*Toolbar_ui.propTypes = {
  classes: PropTypes.object.isRequired,
};*/

export default withTheme()(Toolbar_ui);
