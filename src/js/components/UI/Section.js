var React = require('react')
import $ from "jquery"

import InformationalPane from "../contentDisplay/InformationalPane"
import ClickandDiscover from "../contentDisplay/ClickandDiscover"
import DragNDrop from "../contentDisplay/DragNDrop"
import StepperComp from "../contentDisplay/StepperComp"
import AccordianComp from "../contentDisplay/AccordianComp"
import Assessment from "../contentDisplay/Assessment"
import CaseStudies from "../contentDisplay/CaseStudies"
import Brochure from "../contentDisplay/Brochure"
import Captivate from "../contentDisplay/Captivate"
import Question from "../contentDisplay/Question"
import Carousel from "../contentDisplay/Carousel"
import InteractionBumper from './InteractionBumper'
import SlideandDiscover from '../contentDisplay/SlideandDiscover'

import Avatar from 'material-ui/Avatar'
import Icon from 'material-ui/Icon'

import * as CourseActions from '../../actions/CourseActions'
import CourseStore from '../../stores/CourseStore'

import { withTheme } from 'material-ui/styles';

var VisibilitySensor = require('react-visibility-sensor')

class Section extends React.Component {
	constructor(){
		super()
		this.state = {
			sensorTriggered: false
		}
	}
	getNextThreshold = () => {
	 	let progressID = this.props.sectionId.split("_")
	 	progressID[0] = parseInt(progressID[0]) 
	 	progressID[1] = parseInt(progressID[1])
	 	progressID[2] = parseInt(progressID[2])
	 	CourseActions.updateProgress(progressID)
		CourseActions.loadNextThreshold()
	}
	componentDidUpdate(prevProps, prevState) {
		if(prevState.sensorTriggered == true) {
		 	this.setState({ sensorTriggered: false })
		}
	}
	triggerSensor = (isVisible) => {
		 if(this.props.sectionId != "carousel"){
			 if(isVisible && !this.state.sensorTriggered){
			 	let progressID = this.props.sectionId.split("_")
			 	progressID[0] = parseInt(progressID[0])
			 	progressID[1] = parseInt(progressID[1])
			 	progressID[2] = parseInt(progressID[2])
			 	this.props.threshold === true ? this.getNextThreshold() : CourseActions.updateProgress(progressID)
			 	this.setState({ sensorTriggered: true })
			 }	
		}
	}
	render() {
		const { sectionId, title, children, subTitle, shadowed, directions, style, meta } = this.props

		let that = this
		const styles = {
			section: {
				padding: this.props.hideTitle ? 0 : '20px 0 0 0',
				position: "relative",
				marginBottom: this.props.bottomCaret ? "60px" : "0px",
				//minHeight: "100vh"
			},
			shadowedSection: {
				//minHeight: "100vh",
			    background: shadowed == 'other' ? '#687d1c' : this.props.theme.palette.primary.main,
			    padding: "0px",
			    color: this.props.theme.palette.primary.contrastText,
			    position: "relative",
			    marginBottom: "0px",
			},
			titleCont:{
				display: 'inline-block',
			},
			title:{
				display: 'block',
				margin: 0,
				fontSize: 40,
			    lineHeight: '40px',
			},
			subTitle: {
				display: 'block',
				fontStyle: 'italic',
			},
			avatar: {
				minHeight: 60,
				minWidth: 60,
				display: 'inline-flex',
				verticalAlign: 'top',
				marginRight: 15,
				background: this.props.theme.palette.secondary.main
			},
			icon: {
				fontSize: 40
			}
		}

		const components = {
		    InformationalPane: InformationalPane,
		    ClickandDiscover: ClickandDiscover,
		    DragNDrop: DragNDrop,
		    Assessment: Assessment,
		    CaseStudies: CaseStudies,
		    Brochure: Brochure,
		    Captivate: Captivate,
		    Question: Question,
		    Carousel: Carousel,
		    SlideandDiscover: SlideandDiscover,
		    StepperComp: StepperComp,
		    AccordianComp: AccordianComp
		}

		const icon = meta && ((filter) => {
			switch (filter) {
			case 'ProCoach': return 'record_voice_over'
			case 'ProTip': return 'announcement'
			case 'ProPlay': return 'play_circle_filled'
			case 'ProDrill': return 'directions_run'
			case 'ProTools': return 'work'
			case 'Spinach in the Cookies': return 'art_track'
			case 'ProTIDBIT': return 'speaker_notes'
			case 'Text': return 'format_size'

			default: return 'account_circle'
		}})(meta.filter)
		
		const sectionName = title.replace(/\W+/g, '-').toLowerCase()

		let childrenIndex = 0
		const sectionComponents = _.map( children, function(value, childKey){
			childrenIndex ++
			let subComps =_.map( value, function(component, compKey){
				////console.log(compKey)
				const ComponentType = components[compKey]
	    		return <ComponentType muiTheme={that.props.muiTheme} { ...component } 
    				threshold={that.props.threshold ? that.props.threshold : null} 
    				getNextThreshold={that.props.threshold ? that.getNextThreshold : null} 
    				key={ `${compKey}_${childrenIndex}` } shadowed={shadowed}
    				componentId={`${sectionId}_${childKey}`} parentId={ sectionName } />
	    		})
			return subComps
		})
		return (
			!this.props.threshold ? 
				<VisibilitySensor partialVisibility={'bottom'} delayedCall={true} onChange={this.triggerSensor} scrollDelay={4000}>
					<section id={sectionName} style={shadowed ? { ...style, ...styles.shadowedSection } : { ...style, ...styles.section}}>
						{ !this.props.hideTitle ? <div class="container mdl-grid">
							<div class="mdl-cell mdl-cell--12-col">
								{ meta && <Avatar style={ styles.avatar }><Icon class="material-icons" style={ styles.icon }>{ icon }</Icon></Avatar> }
								<div style={ styles.titleCont }>
									<h2 style={ styles.title }>{title}</h2>
									{ meta ? <p style={ styles.subTitle }>{ meta.filter }</p> : subTitle ? <p style={ styles.subTitle }>{ subTitle }</p> : null }
								</div>
							</div>
						</div> : null }
						{ sectionComponents }
						{ this.props.bottomCaret ? <div class="section-caret"></div> : null }
					</section>
				</VisibilitySensor> 
			: 
				<section id={sectionName} style={shadowed ? { ...style, ...styles.shadowedSection } : { ...style, ...styles.section }}>
					{ !this.props.hideTitle ? <div class="container mdl-grid">
						<div class="mdl-cell mdl-cell--12-col">
								{ meta && <Avatar style={ styles.avatar }><Icon class="material-icons" style={ styles.icon }>{ icon }</Icon></Avatar> }
								<div style={ styles.titleCont }>
									<h2 style={ styles.title }>{title}</h2>
									{ meta ? <p style={ styles.subTitle }>{ meta.filter }</p> : subTitle ? <p style={ styles.subTitle }>{ subTitle }</p> : null }
								</div>
							</div> 
					</div> : null }
					{ sectionComponents }
					{ this.props.bottomCaret ? <div class="section-caret"></div> : null }
					<InteractionBumper style={{background: "#002469", color: "#fff"}} icon={ false } text={ directions } />
				</section> 
			
		)	
	}
}

export default withTheme()(Section);