var React = require('react')
var videojs = require('video.js')
import 'animation.gsap'
import 'debug.addIndicators'
import 'TextPlugin'
var ScrollMagic = require('scrollmagic')
import {TweenMax, TimelineLite, Animation} from "gsap";
import RaisedButton from 'material-ui/RaisedButton';
import ModuleStats from  './ModuleStats'
import FontIcon from 'material-ui/FontIcon';

export default class Banner extends React.Component {
	constructor(){
		super()

		this.state = {
			videoVisible: false
		}
	}
	loadVideo = () => {
		this.setState({
			videoVisible: true
		}, function(){
			this.player = videojs(this.videoNode, this.props.video, function onPlayerReady() { });
		})
	}
	componentDidMount() {
		var controller = new ScrollMagic.Controller({loglevel: 3, addIndicators: true})
		if(this.props.parralaxLayers){
			var paralaxAnimPart = new TimelineMax();
			 _.map(this.props.parralaxLayers, function(value, key){
			 	if(value.direction == "from"){
					paralaxAnimPart.from(`#paralax_layer_${key}`, 1, value.animation, 0);
			 	}else{
			 		paralaxAnimPart.to(`#paralax_layer_${key}`, 1, value.animation, 0);
			 	}
				
			})
			var bg_scene = new ScrollMagic.Scene({
				triggerElement: '#bannerHolder', 
				triggerHook: 0,
				duration: 1000,
			})
		    	.addTo(controller)
		    	.setTween(paralaxAnimPart) 
		}	
		var bannerContentTween = new TimelineMax();
		    bannerContentTween.to('#banner_title', 1, {opacity: 0, top: 100, ease: Power2.easeOut}, 0)
		    bannerContentTween.to('#banner_title_2', 1, {opacity: 0, top: 50, ease: Power2.easeOut}, 0)
		    bannerContentTween.to('#chevron', 1, {opacity: 0, scaleY: 0, scaleX: 0, top: 25, ease: Power2.easeOut}, .3)
 			
		var bannerContent_scene = new ScrollMagic.Scene({
			triggerElement: '#bannerContent', 
			triggerHook: 0,
			duration: 1000,
		})
	    	.addTo(controller)
	    	.setTween(bannerContentTween) 

		var textTimeline = new TimelineMax();
		textTimeline.to("#banner_title", 2, {text: "", ease:Linear.easeNone}, 3);
	   	textTimeline.to("#banner_title", 2, {text: "I'm Ryan and I enjoy:", ease:Linear.easeNone}, 4);

   		var SubTextTimeline = new TimelineMax({repeat: 99, delay: 6});
	   	SubTextTimeline.to("#banner_title_2", 1, {text: {value: "Coding...", padSpace: true}, ease:Linear.easeNone}, 1);
	   	SubTextTimeline.to("#banner_title_2", .8, {text: {value: "", padSpace: true}, ease:Linear.easeNone}, 3);
	   	SubTextTimeline.to("#banner_title_2", 1, {text: {value: "Design...", padSpace: true}, ease:Linear.easeNone}, 4);
	   	SubTextTimeline.to("#banner_title_2", .8, {text: {value: "", padSpace: true}, ease:Linear.easeNone}, 6);
	   	SubTextTimeline.to("#banner_title_2", 1, {text: {value: "eLearning...", padSpace: true}, ease:Linear.easeNone}, 7);
	   	SubTextTimeline.to("#banner_title_2", .8, {text: {value: "", padSpace: true}, ease:Linear.easeNone}, 9);
	    	

	    	
	}

	componentWillUnmount() {
		if (this.player) {
	  		this.player.dispose()
		}
	}
	render() {
		const styles = {
			hero: {
				backgroundImage: `url(${this.props.heroImg})`,
				backgroundSize: "cover",
			   	backgroundPosition: "50% 50%",
			},
			bannerContent: {
				position: "relative",
			},
			banner: {
				height: "auto",
				overflow: "hidden",
				height: "100vh",
				overflow: "hidden",
				backgroundColor: "#205db3",
				//backgroundImage: "url(src/data/course-assets/coding_bg.png)",
				backgroundSize: "cover",
				display: "flex",
				justifyContent: "center",
				alignItems: "center",
				flexFlow: "row wrap",
				flexDirection: "column",
			},
			bannerTitle: {
				marginTop: 50,
				zIndex: 999,
				color: "#fff",
				fontWeight: "bold",
				textShadow: "2px 2px 2px rgba(0,0,0,.3)",
				textAlign: "center",
				position: "relative",
			},
			bannerSubTitle: {
				marginTop: 0,
				color: "#fff",
			},
			bannerSubText: {
				color: "#fff",
			},
			bannerDirections: {
				fontStyle: "italic",
				fontWeight: "bold",
				color: "#fff"
			},
			videoPoster: {
				border: "1px solid #fff",
				maxWidth: "100%",
			},
			paralaxLayer: {
				width: "100%",
				minHeight: "1274px",
				backgroundSize: "cover",
				position: "absolute",
				overflow: "hidden",
				top: 0,
				left: 0,
				zIndex: -1,
			},
			modStats: {
				paddingTop: 80
			},
			absoluteContainer: {
				position: "absolute",
				top: 0,
				left: 0,
				width: "100%",
				height: "1274px",
				overflow: "hidden",
			},
			expand: {
				color: "#205db3",
				fontSize: "3em",
				background: "#fff",
				borderRadius: 99,
				boxShadow: "3px 3px 12px rgba(0,0,0,.3)",
			},
			chevron: {
				position: "relative",
				top: "-60px",
				textAlign: "center",
				marginTop: "-48px",
				

			},
			finger: {
				color: "#fff",
				fontSize: "6em",
			}
		}
		const {videoVisible} = this.state
		const sectionName = this.props.title.replace(/\W+/g, '-').toLowerCase()

		//Paralax Layers
		let paralaxLayers = null
		/*this.props.parralaxLayers ?
			paralaxLayers = _.map(this.props.parralaxLayers, function(value, key){
				return <div id={"paralax_layer_"+key} key={ "paralax_layer_"+ key } style={{ ...styles.paralaxLayer, backgroundImage: `url(${value.src})`}}></div>
			})
		: null*/

		return (
			<div id="bannerHolder">
				<div id={sectionName} style={ this.props.heroImg ? { ...styles.banner, ...styles.hero}  :  styles.banner } key="banner">
					<div class="container" id="bannerContent" style={styles.bannerContent}>
						<div class="mdl-grid">
							<div class="mdl-cell mdl-cell--12-col" style={{textAlign: "center"}}>
								<FontIcon className="material-icons" style={styles.finger}>fingerprint</FontIcon>
								<h1 style={styles.bannerTitle} id="banner_title"> Hi there. </h1>
								<h1 style={styles.bannerTitle} id="banner_title_2">&nbsp; </h1>
								
							</div>
						</div>
					</div>
				</div>
				<div style={styles.chevron} id="chevron">
					<div><FontIcon className="material-icons" style={styles.expand}>expand_more</FontIcon></div>
				</div>
			</div>
		)	
	}
}
