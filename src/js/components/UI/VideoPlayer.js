var React = require('react')
var videojs = require('video.js')

import MediaStore from '../../stores/MediaStore'

export default class VideoPlayer extends React.Component {
	constructor (props) {
		super(props)
		this.state = {
			playing: false,
			videoVisible: false
		}
	}
	 componentWillUnmount() {
		if (this.player) {
	  		this.player.dispose()
		}
		MediaStore.removeListener('stop_media', this.pausePlayer)
	}
	componentDidMount () {
		const videoJsOptions = {
			preload: false,
			poster: this.props.data.poster,
			autoplay: false,
			controls: true,
			sources: [{
				src: this.props.data.url,
				type: 'video/mp4'
			}]
		}
		let that = this
		this.player = videojs( this.videoNode, videoJsOptions, function onPlayerReady() { console.log("Video Player Ready") });
		this.player.on("play", function(){
			MediaStore.stopMedia(that.props.componentId)
		})
		MediaStore.on('stop_media', this.pausePlayer)
	}

	pausePlayer = () => {
		if(MediaStore.activeMedia != this.props.componentId){
			this.player.pause()
		}
	}

	render() {
		const { videoVisible } = this.state
		const styles = {
			playBtn:{
				float: "left", 
			}
		}
		return (
			<div data-vjs-player>
				<video ref={ node => this.videoNode = node } className="video-js vjs-fluid"></video>
			</div>
		)	
	}
}
