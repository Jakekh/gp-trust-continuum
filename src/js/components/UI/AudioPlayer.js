var React = require('react')
import ReactHowler from 'react-howler'
import raf from 'raf' // requestAnimationFrame polyfill

import Icon from 'material-ui/Icon'
import Button from 'material-ui/Button'
import { LinearProgress } from 'material-ui/Progress';
import MediaStore from '../../stores/MediaStore'

export default class AudioPlayer extends React.Component {
	constructor (props) {
		super(props)

		this.state = {
			playing: false,
			loaded: false,
			loop: false,
			mute: false,
			volume: 1.0,
			seek: 0,
			duration: 0,
		}
		this.handleToggle = this.handleToggle.bind(this)
		this.handleOnLoad = this.handleOnLoad.bind(this)
		this.handleOnEnd = this.handleOnEnd.bind(this)
		this.handleOnPlay = this.handleOnPlay.bind(this)
		this.handleStop = this.handleStop.bind(this)
		this.renderSeekPos = this.renderSeekPos.bind(this)
		this.handleLoopToggle = this.handleLoopToggle.bind(this)
		this.handleMuteToggle = this.handleMuteToggle.bind(this)
	}
	componentWillUnmount () {
		this.clearRAF()
		MediaStore.removeListener('stop_media', this.checkStop)
	}
	componentDidMount () {
		MediaStore.on('stop_media', this.checkStop) 
	}
	checkStop = () => {
		 this.state.playing ? this.handleStop() : null 
	}
	handleToggle () {
		MediaStore.stopMedia()
		this.setState({
			playing: !this.state.playing
		})
	}

	handleOnLoad () {
		this.setState({
			loaded: true,
			duration: this.player.duration()
		})
	}

	handleOnPlay () {
		this.setState({
			playing: true
		})
		this.renderSeekPos()
	}

	handleOnEnd () {
		this.setState({
			playing: false
		})
		this.clearRAF()
	}

	handleStop () {
		//console.log("Stop...")
		if(this.player ){
			this.player.stop() 
			this.setState({
	      		playing: false // Need to update our local state so we don't immediately invoke autoplay
	    	})
			this.renderSeekPos()
		}
	}

	handleLoopToggle () {
		this.setState({
			loop: !this.state.loop
		})
	}

	handleMuteToggle () {
		this.setState({
			mute: !this.state.mute
		})
	}

	renderSeekPos () {
		this.setState({
			seek: this.player.seek()
		})
		if (this.state.playing) {
			this._raf = raf(this.renderSeekPos)
		}
	}

	seekAudio = (e) => {
		var rect = e.target.getBoundingClientRect()
		var x = e.nativeEvent.clientX - rect.left 
		this.player.seek( this.state.duration * x / rect.width )
		this.setState({
			seek: this.player.seek()
		})
	}
	clearRAF () {
		raf.cancel(this._raf)
	}

	render() {
		console.log("AUDIO", this.state.seek)
		const styles = {
			playBtn:{
				flex: 1
			},
			seekbar:{
				flex: 5,
				width: "auto",
				paddingTop: "29px",
			},
			listenText: {
				position: "relative",
				bottom: "16px",
				padding: "8px",
			},
			seekBarHandle: {
				cursor: "pointer",
				transition: "none",
			},
			audioRoot: {
				display: "flex",
				padding: 10

			}
		}
		return (
			<div className='full-control' style={styles.audioRoot}>
				{ this.state.playing && <ReactHowler
					src={[this.props.data.url]}
					playing={this.state.playing}
					onLoad={this.handleOnLoad}
					onPlay={this.handleOnPlay}
					onEnd={this.handleOnEnd}
					loop={this.state.loop}
					mute={this.state.mute}
					volume={this.state.volume}
					ref={(ref) => (this.player = ref)} /> 
				}

				<div style={styles.playBtn}>
					<Button variant="fab" onTouchTap={this.handleToggle} color='primary'>
						<Icon className="material-icons">{ !this.state.playing ? "play_arrow" : "pause" }</Icon>
					</Button>
				</div>
				<div style={styles.seekbar}>
					<LinearProgress style={styles.seekBarHandle} class="audioSeekbar" onTouchTap={this.seekAudio.bind(this)} variant="determinate" value={ this.state.seek && this.state.seek != undefined  ? parseInt(this.state.seek / this.state.duration * 100).toFixed(2) : 0} />
				</div>
			</div>
			)	
	}
}
