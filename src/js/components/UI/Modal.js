var React = require('react')
var $ = require('jquery')

import Icon from 'material-ui/Icon'
import IconButton from 'material-ui/IconButton'

import ModalStore from '../../stores/ModalStore'
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'

import Button from 'material-ui/Button'
export default class Modal extends React.Component {

	constructor(){
		super()
		this.state = {
			open: false,
			content: []
		}
	}

	componentWillMount = () => {
		ModalStore.on('modal-opened', () => {this.setState({open: true, content: ModalStore.content, style: ModalStore.style})})
	}

	componentWillUnmount = () => {
		ModalStore.removeAllListeners()
	}

	handleClose = () => {
	    this.setState({open: false})
	}

	render() {
		const { open, content, style } = this.state
		//console.log(content)

		let modal
		const styles = {
			modal: {
				backgroundSize: "cover",
			   	backgroundPosition: "center",
			    backgroundRepeat: 'no-repeat',
			    backgroundColor: '#fff',
			    bottom: 0,
			    left: 0,
			    position: 'fixed',
			    right: 0,
			    top: 0,
			    zIndex: 9999,
			    margin: 0,
			    padding: 0,
			    overflowX: "hidden",
			},
			content: {
				bottom: 0,
			    left: 0,
			    position: 'absolute',
			    right: 0,
			    top: 0,
			    overflowX: 'hidden',
			},
			close: {
			    display: 'inline-block',
			    top: 20,
			    position: 'absolute',
			    right: 20,
			    zIndex: 100,
			    color: '#fff',
			},
		}
		if(open){
			$('body').css('overflow', 'hidden')
			modal = 	<div style={ { ...styles.modal, ...style } } >
							<div class="fullScreenModal" style={ styles.content }>{ content }</div>
							<IconButton onTouchTap={ this.handleClose } style={ styles.close }>
								<Icon class="material-icons">close</Icon>
							</IconButton>
						</div>
		}else{
			$('body').css('overflow', 'auto')
			modal = null
		}

		return (
				<CSSTransitionGroup
		          transitionName="modal"
		          transitionEnterTimeout={500}
		          transitionLeaveTimeout={300}>{ modal }
		          </CSSTransitionGroup>
		)
	}
}