var React = require('react')
var $ = require('jquery-easing')

import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'

import Icon from 'material-ui/Icon'
import ButtonBase from 'material-ui/ButtonBase'

import * as CourseActions from '../../actions/CourseActions'
import CourseStore from '../../stores/CourseStore'
import LmsStore from '../../stores/LmsStore'
import AssetStore from '../../stores/AssetStore'

const styles = theme => ({
	button: {
	    position: 'relative',
        display: 'block',
	    width: '100%',
	    height: '100%',
	    '&:hover': {
	      	zIndex: 1,
	    },
	    '&:hover $imageBackdrop': {
	      	opacity: 0.15,
	    },
	},
	imageBackdrop: {
	    position: 'absolute',
	    left: 0,
	    right: 0,
	    top: 0,
	    bottom: 0,
	    backgroundColor: theme.palette.common.black,
	    opacity: 0,
	    transition: theme.transitions.create('opacity'),
	},
})

class Footer extends React.Component {
	componentDidMount() {
		const location = LmsStore.returnLocation()
		const moduleComplete = LmsStore.getModuleProgress()[location[0]]
		if(!moduleComplete){
			const currentPos = $(window).scrollTop()
			/*$("html, body").stop().animate({ scrollTop: currentPos + 265  }, 600, 'easeInOutQuad', () => {
				setTimeout(() => {
					$(window).on("scroll", function(){ $('#footer-icon').fadeOut() })
				}, 500)
			})*/
			LmsStore.setModuleCopmlete(location[0])
		}else{
			//$('#footer-icon').fadeOut()
		}
		let progressID = this.props.sectionId.split("_")
	 	progressID[0] = parseInt(progressID[0]) 
	 	progressID[1] = parseInt(progressID[1])
	 	progressID[2] = parseInt(progressID[2])
	 	console.log("PROGRESSID", progressID)
	 	CourseActions.updateProgress(progressID)
	}

	back = () => {
		const location = LmsStore.returnLocation()
		const courseData = CourseStore.returnCourseData()
		courseData.modules[location[0]].lessons[location[1]-1] ? CourseActions.loadModule({ module: location[0], lesson: location[1]-1 }) : $("html, body").animate({ scrollTop: 0}, 500, "swing")
	}

	next = () => {
		const location = LmsStore.returnLocation()
		const courseData = CourseStore.returnCourseData()
		console.log(courseData.modules[location[0]])
		courseData.modules[location[0]].lessons[location[1]+1] ? CourseActions.loadModule({ module: location[0], lesson: location[1]+1 }) : $("html, body").animate({ scrollTop: 0}, 500, "swing")
	}

	render() {
	    const { classes } = this.props
		const location = LmsStore.returnLocation()
		
		const courseData = CourseStore.returnCourseData()
		const lastSection = courseData.modules[location[0]].lessons[location[1]-1] ? true : false
		const nextSection = courseData.modules[location[0]].lessons[location[1]+1] ? true : false
		const lastSectionObj = courseData.modules[location[0]].lessons[location[1]-1] ? courseData.modules[location[0]].lessons[location[1]-1].sections[0].Banner : courseData.modules[location[0]].lessons[location[1]].sections[0].Banner
		const nextSectionObj = courseData.modules[location[0]].lessons[location[1]+1] ? courseData.modules[location[0]].lessons[location[1]+1].sections[0].Banner : courseData.modules[location[0]].lessons[location[1]].sections[0].Banner

		const hasNext = courseData.modules[location[0]].lessons[location[1]+1] ? true : false
		const hasPrev = courseData.modules[location[0]].lessons[location[1]-1] ? true : false

		const styles = {
			fixedContainer: {
				background: "#252525",
				width: "100%",
			},
			container: {
				maxWidth: 1200,
				margin: "0 auto",
			},
			backNextBtn: {
				color: '#fff',
				display: "inline-flex",
				padding: "30px 0",
			},
			h4: {
				margin: 0,
				textShadow: "2px 2px 2px rgba(0,0,0,.5)",
			},
			h5: {
				margin: 0,
				textShadow: "2px 2px 2px rgba(0,0,0,.5)",
				fontWeight: 100,
			},
			chevron: {
				fontSize: "60px"
			},
			menu: {
				fontSize: "40px",
			},
			assetBtn: {
				background: "#333",
				textAlign: "center",
				color: "#fff",
			}
		}

		this.backBtn = <ButtonBase focusRipple class={ classes.button } style={ styles.backNextBtn } onTouchTap={ this.back }>
							<div style={{flex: 1}}>
								<Icon class="material-icons" style={styles.chevron}>chevron_left</Icon>
							</div>
							<div style={{flex: 8}}>
								<h4 style={{...styles.h4, textAlign: "left"}}>{ hasPrev ? "BACK" : "RESTART" }</h4>
								<h5 style={{...styles.h5, textAlign: "left"}}> {lastSectionObj.title} </h5> 
							</div>
							<span class={ classes.imageBackdrop } />
						</ButtonBase>
		this.nextBtn = 
						<ButtonBase focusRipple class={ classes.button } style={ styles.backNextBtn } onTouchTap={ this.next }>
							<div style={{flex: 8}}>
								<h4 style={{...styles.h4, textAlign: "right"}}>NEXT</h4>
								<h5 style={{...styles.h5, textAlign: "right"}}> {nextSectionObj.title} </h5> 
							</div>
							<div style={{flex: 1}}>
								<Icon class="material-icons" style={styles.chevron}>chevron_right</Icon>
							</div>
							<span class={ classes.imageBackdrop } />
						</ButtonBase>
        this.assetBtn = 
						<ButtonBase focusRipple class={ classes.button } onTouchTap={ AssetStore.createResultsModal }>
							<Icon class="material-icons" style={styles.menu}>menu</Icon>
							<h5 style={{...styles.h5, textAlign: 'center'}}>ASSET BANK</h5>
							<span class={ classes.imageBackdrop } />
						</ButtonBase>

		const sectionName = this.props.title.replace(/\W+/g, '-').toLowerCase()

		return (
			<div id={ sectionName } key="footer">
				<div style={ styles.fixedContainer }>
					<div style={ styles.container }>
						<div class="mdl-grid mdl-grid--no-spacing">
							<div class="mdl-cell mdl-cell--12-col">
							</div>
							<div class="mdl-cell mdl-cell--5-col">
								{ this.backBtn }
							</div>
							<div style={ styles.assetBtn } class="mdl-cell mdl-cell--2-col">
								{ this.assetBtn }
							</div>
							<div class="mdl-cell mdl-cell--5-col">
								{ hasNext ? this.nextBtn : null }
							</div>
						</div>
					</div>
				</div>
			</div>
		)	
	}
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles, { withTheme: true })(Footer)
