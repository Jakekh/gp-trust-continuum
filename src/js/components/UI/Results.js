var React = require('react')
var $ = require('jquery')

import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'

import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'
import Card, { CardActions, CardContent } from 'material-ui/Card'
import IconButton from 'material-ui/IconButton'
import Button from 'material-ui/Button'
import Typography from 'material-ui/Typography'
import Input, { InputLabel, InputAdornment } from 'material-ui/Input';
import TextField from 'material-ui/TextField'
import Icon from 'material-ui/Icon'
import Avatar from 'material-ui/Avatar'

import SingleView from './SingleView'

import LmsStore from '../../stores/LmsStore'
import AssetStore from '../../stores/AssetStore'
import CourseStore from '../../stores/CourseStore'

import * as AssetActions from '../../actions/AssetActions'

const styles = theme => ({
    CardContent: {
        position: 'relative',
        textAlign: 'center',
    },
    CardActions: {
        marginTop: 'auto',
    },
  	filter: {
    	marginBottom: 16,
    	fontSize: 14,
		display: 'inline-flex',
  	},
  	statusComplete: {
  		color: 'green',
    	fontSize: 14,
    	position: 'absolute',
    	right: 16,
    	top: 16,
  	},
  	statusIncomplete: {
  		color: 'red',
    	fontSize: 14,
    	position: 'absolute',
    	right: 16,
    	top: 16,
  	},
  	status: {
  		display: 'flex',
  	},
	TextField: {
	    marginTop: 8,
	},
})

class Results extends React.Component {
	constructor(){
		super()
		this.state = {
			open: false,
			content: [],
			search: '',
			pageView: false,
			currentAsset: null
		}
	}

	componentWillMount = () => {
		LmsStore.on("user_progress_updated", this.update)
		AssetStore.on('show-results', this.handleOpen)
		AssetStore.on("stop-search", this.handleClose)
	}

	componentWillUnmount = () => {
		AssetStore.removeAllListeners()
	}

	update = () => {
		AssetStore.getAssets((assets) => {
			this.filterContent(assets)
		})
		if(!this.state.hasContent){
			this.setState({hasContent: true})
		}
	}

	handleOpen = () => {
	    this.setState({open: true}, () => {
	    	!this.state.hasContent && this.update()
	    })
	    $('body').addClass('block-scroll')
	}

	handleClose = () => {
	    this.setState({open: false})
	    $('body').removeClass('block-scroll')
	}

	toggleView = () => {
	    this.setState({pageView: !this.state.pageView})
	}

	launch = data => {
		this.setState({ currentAsset: data, pageView: !this.state.pageView })
	}

	handleChange = event => {
		this.setState({ search: event.target.value }, () => {
			this.filterContent(AssetStore.assets)
		})
	}

	filterContent = (data) => {
		let matches = []
		const { search } = this.state
		_.map(data, (section, key) => {
			let sectionData = _.assign({}, section.meta)
			delete sectionData.status
			delete sectionData.sectionId
			_.map(sectionData, (value, key) => {
				const metaItem = value.toUpperCase()
				const searchVal = search == '' ? null : search.toUpperCase()
				metaItem.match(searchVal) && !_.includes(matches, section) && matches.push(section)
			})
		})
		this.setState({content: search == "" ? data : matches})
	}

	pickIcon = (filter) => {
		switch (filter) {
		case 'ProCoach': return 'record_voice_over'
		case 'ProTip': return 'announcement'
		case 'ProPlay': return 'play_circle_filled'
		case 'ProDrill': return 'directions_run'
		case 'ProTools': return 'work'
		case 'Spinach in the Cookies': return 'art_track'
		case 'ProTIDBIT': return 'speaker_notes'
		case 'Text': return 'format_size'

		default: return 'account_circle'
	}}

	render() {
		const that = this
		const { open, content, pageView, currentAsset } = this.state
		const { classes } = this.props
		const styles = {
			modal: {
			    background: '#f3f3f3',
			    bottom: 0,
			    left: 0,
			    position: 'fixed',
			    right: 0,
			    top: 65,
			    zIndex: 1000,
			    margin: 0,
			    padding: 0,
			    overflowX: "hidden",
			},
			content: {
				bottom: 0,
			    left: 0,
			    position: 'absolute',
			    right: 0,
			    top: 0,
		        padding: '0 10px',
		        paddingLeft: CourseStore.menuOpen ? "300px" : 0,
			},
			thumbs: {
				overflowY: 'auto',
			    width: '100%',
		        maxHeight: 'calc(100vh - 180px)',
			},
			close: {
			    display: 'inline-block',
			    top: 20,
			    position: 'absolute',
			    right: 25,
			    zIndex: 100,
			},
			back: {
			    display: 'inline-block',
			    top: 20,
			    position: 'absolute',
			    left: 25,
			    zIndex: 100,
			},
			card: {
		        display: 'flex',
		        flexDirection: 'column',
		        cursor: "pointer",
			},
			avatar: {
				height: 100,
				width: 100,
				verticalAlign: 'top',
				margin: 'auto',
				background: this.props.theme.palette.secondary.main,
			},
			icon: {
				fontSize: 50
			},
		}

		const thumbs = _.map(content, (value, key) => {
			const { meta } = value
			return <Card key={ `thumb-${key}` } class="mdl-cell mdl-cell--2-col mdl-cell--4-col-tablet mdl-cell--4-col-tablet" style={ styles.card }>
				<CardContent onTouchTap={ this.launch.bind(this, value) } class={ classes.CardContent }>
					<Avatar style={ styles.avatar }><Icon class="material-icons" style={ styles.icon }>{ this.pickIcon(meta.filter) }</Icon></Avatar>
					<Typography class={ classes.filter } color="textSecondary">{ meta.filter }</Typography>
					<Typography class={ meta.status == 'complete' ? classes.statusComplete : classes.statusIncomplete } color="textSecondary">
						{ meta.status == 'complete' && <Icon>{ meta.status == 'complete' ? 'check_circle' : 'cancel' }</Icon> }
					</Typography>
					<Typography variant="headline" component="h2">{ meta.title }</Typography>
				</CardContent>
			</Card>
		})

		const modal = 
			pageView ?
				<div style={ styles.modal } >
					<div class="fullScreenModal" id="assetBank" style={ styles.content }>
						<SingleView data={ currentAsset } />
					</div>							
					<IconButton onTouchTap={ this.toggleView } style={ styles.back }>
						<Icon>arrow_back</Icon>
					</IconButton>							
					<IconButton onTouchTap={ AssetActions.stopSearch } style={ styles.close }>
						<Icon>close</Icon>
					</IconButton>
				</div>
			:
				<div style={ styles.modal } >
					<div class="fullScreenModal" id="assetBank" style={ styles.content }>
						<div class="mdl-grid">
							<div class='mdl-cell mdl-cell--12-col'>
								<div class="mdl-cell mdl-cell--6-col">
									<TextField autoFocus={ true } value={this.state.search} fullWidth={ true } id="search" label="Asset Bank Search" onChange={ this.handleChange } />
								</div>
								<div class="mdl-cell mdl-cell--6-col">
								</div>
							</div>
							<div class="mdl-grid" style={ styles.thumbs }>{ thumbs }</div>
						</div>
					</div>							
					<IconButton onTouchTap={ AssetActions.stopSearch } style={ styles.close }>
						<Icon>close</Icon>
					</IconButton>
				</div>

		return (
				<CSSTransitionGroup
		          transitionName="modal"
		          transitionEnterTimeout={500}
		          transitionLeaveTimeout={300}>{ open && modal }
		        </CSSTransitionGroup>
		)
	}
}

Results.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles, {withTheme: true})(Results)