var React = require('react')
var $ = require('jquery-easing')
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper'
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import Icon from 'material-ui/Icon'
import Toolbar from 'material-ui/Toolbar'
import IconButton from 'material-ui/IconButton'
import Divider from 'material-ui/Divider'
import Drawer from 'material-ui/Drawer'
import Collapse from 'material-ui/transitions/Collapse';
import * as CourseActions from '../../actions/CourseActions'
import CourseStore from '../../stores/CourseStore'
import LmsStore from '../../stores/LmsStore'
import { withTheme } from 'material-ui/styles';
import * as AssetActions from '../../actions/AssetActions'

const update = require('immutability-helper')

const styles = {

}


class Sidebar extends React.Component {
	constructor(){
		super()
		let lessonOpenAry = []
		 _.map(CourseStore.returnCourseData().modules, function(mod, index){
		 	lessonOpenAry.push({item: index, open: false, lessons: []})
		 	_.map(mod.lessons, function(lesson, lessonIndex){
				lessonOpenAry[index].lessons.push({item: lessonIndex, open: false})
			})
		})
		 //console.log("CURRENT LOCATION: ", LmsStore.returnLocation())
		this.state = {
			view: CourseStore.returnCourseData(),
			location: LmsStore.returnLocation(),
			moduleProgress: [],
			menuOpen: false,
			windowWidth: $(window).width(),
			openModules: lessonOpenAry,
		}
	}
	
	loadLocation = (modKey, lessKey) => {
		const { activeSection } = this.state
		if( LmsStore.returnLocation()[0] != modKey || LmsStore.returnLocation()[1] != lessKey ){
			CourseActions.loadModule({ module: modKey, lesson: lessKey })
			$(window).scrollTop(0)
		}else{
			if(activeSection != null && activeSection != ""){
				$("html, body").animate({ scrollTop: 0}, 500, "swing")
			}
		}
		this.state.menuOpen ? this.toggleMenu() : null
	}
	resize = () => {
		const { windowWidth } = this.state
		if(windowWidth > 600){
			
			if($(window).width() < 600){
				////console.log("Re-render")
				this.setState({
					windowWidth : $(window).width()
				})
			}
		}else{
			if($(window).width() > 600){
				////console.log("Re-render")
				this.setState({
					windowWidth : $(window).width()
				})
			}
		}
	}
	
	componentDidMount() {
		let that = this
		CourseStore.on("menuToggle", function(){that.setState({ menuOpen: CourseStore.menuOpen })})
		CourseStore.on("view_changed", this._updateLocation)
		LmsStore.on("module_completed", this._updateProgress)
		LmsStore.on("lms_data_loaded", this._updateProgress)
		LmsStore.on("user_progress_updated", this._updateProgress)
		window.addEventListener('resize', this.resize);
	}
	componentWillUnmount(){
		let that = this
		CourseStore.removeListener("menuToggle", function(){that.setState({ menuOpen: CourseStore.menuOpen })})
	}
	
	linkScroll =() => {
		let that = this
		$(window).off("scroll")
		setTimeout(function(){
				let topMenu = $("#sidebarSections"),
			    topMenuHeight = topMenu.outerHeight()+15,
			    menuItems = topMenu.find("li"),
			    scrollItems = menuItems.map(function(){
			      const label = $(this).attr("data-scroll-label").toString()
			      const item = $(`#${label}`)
			      if(item.length) { return item }
			    })
			// Bind to scroll
			$(window).on("scroll", function(){
			   // Get container scroll position
			   let fromTop = $(this).scrollTop()

			   // Get id of current scroll item
			   let cur = scrollItems.map(function(){
			     if ($(this).offset().top < fromTop + 300)
			       return this
			   })
			   // Get the id of the current element
			   cur = cur[cur.length-1]
			   let id = cur && cur.length ? cur[0].id : ""
			   // Set/remove active class
			   //////console.log(id)
			   if(that.state.activeSection != id){
			   	that.setState({
			   		activeSection : id
			   	})
			   }
			})
		}, 200)
	}

	_updateLocation = () => {
		this.setState({
			location: LmsStore.returnLocation(),
			activeSection: null,
		}, function(){
			this.linkScroll()
		})
	}

	_updateProgress = () => {
		//console.log("NEW PROGRESS: ", LmsStore.getModuleProgress() )
		this.setState({moduleProgress: LmsStore.getModuleProgress()})
	}

	toggleMenu = () => {
		CourseStore.toggleMenu()
	}

	goToSection = (sectionTitle, mod, less) => {
		AssetActions.stopSearch()
		//console.log("MOD: ", mod, "LESS: ", less, "loc:", LmsStore.returnLocation())
		let secId = sectionTitle.replace(/\W+/g, '-').toLowerCase()
		if( LmsStore.returnLocation()[0] === mod && LmsStore.returnLocation()[1] === less ){
			if($('#'+secId).length){
				$("html, body").off().stop().animate({ scrollTop: $('#'+secId).offset().top - 56}, 1000, "swing")
			}
		}else{
			this.loadLocation(mod, less, secId)
			if($('#'+secId).length){
				setTimeout(function(){ $("html, body").off().stop().animate({ scrollTop: $('#'+secId).offset().top - 56}, 1000, "swing") }, 200)
			}
		}
	}
	getProgressIcon = (data) => {
		const styles = {
			sec_complete: {
				color: "green",
			},
			sec_incomplete: {
				color: "#ccc",
				zIndex: "999",
			},
			mod_complete: {
				color: this.props.theme.palette.primary.main,
			},
			mod_incomplete: {
				color: "#ccc",
				zIndex: "999",
			},
		}

		let sectionIcons = {
			unViewed: <Icon className="material-icons" style={styles.sec_incomplete}>radio_button_unchecked</Icon>,
			viewed: <Icon className="material-icons" style={styles.sec_incomplete}>radio_button_checked</Icon>,
			complete: <Icon className="material-icons" style={styles.sec_complete}>check_circle</Icon>,
			incomplete: <Icon className="material-icons" style={styles.sec_incomplete}>check_circle</Icon>
		}
		let modIcons = {
			unViewed: <Icon className="material-icons" style={styles.mod_incomplete}>radio_button_unchecked</Icon>,
			viewed: <Icon className="material-icons" style={styles.mod_incomplete}>radio_button_checked</Icon>,
			complete: <Icon className="material-icons" style={styles.mod_complete}>check_circle</Icon>,
			incomplete: <Icon className="material-icons" style={styles.mod_incomplete}>check_circle</Icon>
		}


		let courseProgress = LmsStore.getProgress()
		//console.log("CourseProg: ", courseProgress, data)
		let complete = false

		if(courseProgress && courseProgress.length){
			if(data.module != null && data.lesson != null && data.section != null){
				//section progress
				let sectionIndex = 0
				let sectionLocation = 0
				let activeThresholdIndex = 0
				let keyLoc = 0
				let matchFound = false

				let lessonProgress = courseProgress[data.module][data.lesson]
				_.each(lessonProgress, function(threshold, threshKey){
					if(matchFound){
						return true
					}
					_.each(threshold, function(section, secKey){
						if(data.section == sectionIndex){
							//console.log("FOUND: ", data.section )
							activeThresholdIndex = threshKey
							keyLoc = sectionLocation
							matchFound = true
							return false
						}
						sectionIndex ++
						sectionLocation ++
					})
					sectionLocation = 0 
				})

				complete = courseProgress[data.module][data.lesson][activeThresholdIndex][keyLoc] == 1 ? true : false

			}else if(data.module != null && data.lesson != null && !data.section){
				//lesson progress
				let tmpMod = _.flattenDeep(courseProgress[data.module][data.lesson]) 
				complete = tmpMod.indexOf(0) == -1 ? true : false

			}else if(data.module != null && !data.lesson && !data.section){
				//module progress
				let tmpMod = _.flattenDeep(courseProgress[data.module]) 

				tmpMod = _.union(tmpMod)
				//console.log("tmpMod: ", tmpMod)
				complete = tmpMod.length == 1 && tmpMod[0] == 1 ? true : false

			}
		}
		
		//console.log("COMPLETE: ", complete)

		let icon

		complete ?
			!data.section ?
				icon = modIcons.complete
			:
				icon = sectionIcons.complete
		:
			!data.section ?
				icon = modIcons.incomplete
			:
				icon = sectionIcons.incomplete

		return icon
	}
	expandMod = (key) => {
		let open = this.state.openModules[key].open
		let openModulesTemp = update( this.state.openModules, { [key]: { open: { $set: !open} }})
		this.setState({
			openModules: openModulesTemp
		})
	}
	expandLes = (mod, key) => {
		//console.log("Expanding: ", mod, key)
		let open = this.state.openModules[mod].lessons[key].open
		let openLessonsTemp = update( this.state.openModules, { [mod]: { lessons:  { [key]: { $set: { open: !open } }}}})
		this.setState({
			openModules: openLessonsTemp
		})
	}
	buildMenu = (data) => {
		let menuStyles={
			title: {
				
			}
		}
		let that = this
		let rootEls = _.map(data, function(module, key){
			//console.log("BUILDING MENU: ", data)
			let progIcon = that.getProgressIcon({module: key, lesson: null, section: null})
			return 	<div key={`list_${key}`}>
						<ListItem divider={true} key={`mod_menu_${key}`} >
				          <ListItemIcon>
				            {progIcon}
				          </ListItemIcon>
				          <ListItemText classes={menuStyles.title} primary={module.title} />
				          { that.state.openModules[key].open ? <IconButton onTouchTap={that.expandMod.bind(this, key)}><Icon>expand_less</Icon></IconButton> : <IconButton onTouchTap={that.expandMod.bind(this, key)}><Icon>expand_more</Icon></IconButton>}
				        </ListItem>
				        { that.buildLessons(key, module.lessons) }
			        </div>


		})
		return rootEls
	}
	buildLessons = (mod, data) => {
		let menuStyles={
			title: {
				
			},
			lesson: {
				background: "#f3f3f3"
			}
		}
		let that = this
		const { location, openModules } = this.state
		let progIcon = null
		//let rootEls = _.map(data, function(lesson, key){
			
		
		return 	<Collapse in={ !openModules[mod].open ? false : true } timeout="auto" unmountOnExit key={`col_less_${mod}`}>
					<List component="div" disablePadding style={menuStyles.lesson} key={`list_less_${mod}`}>
						{ 
							_.map(data, function(lesson, key){
								progIcon = that.getProgressIcon({module: mod, lesson: key, section: null})
								return <div key={`div_lesson_menu_${mod}_${key}`}>
									<ListItem divider={true} key={`lesson_menu_${mod}_${key}`} >
										<ListItemIcon>
											{ progIcon }
										</ListItemIcon>
										<ListItemText classes={ menuStyles.title } primary={ lesson.title } />
										{ that.state.openModules[mod].lessons[key].open ? <IconButton onTouchTap={that.expandLes.bind(this,mod, key)}><Icon>expand_less</Icon></IconButton> : <IconButton onTouchTap={that.expandLes.bind(this, mod, key)}><Icon>expand_more</Icon></IconButton>}
									</ListItem>
									{ that.buildSections(mod, key, lesson.sections) }
								</div>
							})
						}
					</List>
				</Collapse>


		//})
		return rootEls
	}
	buildSections = (mod, less, sections) => {
		let menuStyles={
			icon: {
				margin: "0px",
				color: "#f00"
			},
			title: {
				fontSize: "18px",
			},
			section: {
				background: "#e2e2e2"
			}
		}
		const { location, openModules } = this.state
		let that = this
		let sectionEl
		let progIcon = null
		
		return <Collapse in={ !openModules[mod].lessons[less].open ? false : true } timeout="auto" unmountOnExit key={`mod_${mod}_less_${less}_sections`}>
			<List component="div" disablePadding style={menuStyles.section}>
			{ 
				_.map(sections, function(section, key){
					if( section.Section ){
						progIcon = that.getProgressIcon({module: mod, lesson: less, section: key})
						return <ListItem key={`section_menu_${mod}_${less}_${key}`} button divider={true} onTouchTap={ that.goToSection.bind(that, section[Object.keys(section)[0]].title, mod, less) } >
							<ListItemIcon>
								{ progIcon }
							</ListItemIcon>
							<ListItemText primary={ section[Object.keys(section)[0]].title } />
						</ListItem>
					}
				})
			}
			</List>
		</Collapse>
		
	}
	render() {
		let that = this
		const { view, location, activeSection, menuOpen } = this.state
		const { classes } = this.props
		const primaryColor = "#0099CC"
		const displayWidth = $(window).width()
		//console.log(this.props)

		const styles = {
			drawer: {
				/*height: `calc(100vh - 48px`,
				top: "48px",*/
				background: "#fff",
			},
			toolbar: {
				background: "#fff",
				height: "48px",
			},
			courseTitle: {
				lineHeight: "48px",
				paddingLeft: "20px",
			},
			root: {
			    width: '100%',
			  },
			  flex: {
			    flex: 1,
			  },
			  menuButton: {
			    marginLeft: -12,
			    marginRight: 20,
			  },
		}
		
		
		let listItems = this.buildMenu(view.modules)

		return (
			location ?
				<Drawer variant="persistent" width={300} open={menuOpen}>
					<Toolbar style={styles.toolbar}>
						<Typography variant="title" color="inherit" style={styles.flex}>
				            Menu
			          	</Typography>
						<IconButton onTouchTap={this.toggleMenu}>
					    	<Icon class="material-icons">close</Icon>
					    </IconButton>
				    </Toolbar>
				    <Divider />
				    <div style={{width: 300}}>
						<List key={ "sidebar_0" }>
							{ listItems }
						</List>
					</div>
				</Drawer>
			:
			<div style={styles.sidebarContainer}>
				<p>Loading...</p>
			</div>	
		)	
	}
}

/*Sidebar.propTypes = {
  classes: PropTypes.object.isRequired,
};*/

export default withTheme(styles)(Sidebar);