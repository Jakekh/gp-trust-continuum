var React = require('react')
var videojs = require('video.js')

import Icon from 'material-ui/Icon';

export default class ModuleStats extends React.Component {
	componentWillUnmount() {
		
	}
	render() {
		const styles = {
			container: {
				textAlign: "center",
				maxWidth: 600,
				margin: "0 auto",
				padding: "10px",
				color: "#fff",
			},
			moduleStats: {
				position: "relative",
				border: "1px solid #fff",
				borderTop: "none",
				minHeight: "50px",
				marginTop: "40px",
				padding: "18px 0px 12px",
			},
			legendText: {
				position: "absolute",
				width: "100%",
				overflow: "hidden",
				top: "-11px",
				textTransform: "uppercase",
			},
			statLabel: {
				lineHeight: "24px",
				position: "relative",
				top: "-7px",
			},
			modStat: {
				display: "inline-block",
				marginTop: "5px",
				padding: "6px 12px",
			}
		}
		return (
			<div style={styles.container}>
				<div style={styles.moduleStats}>
					<p style={styles.legendText} class="legendText">Chapter Stats</p>
						<div style={styles.modStat}>
							<Icon className="material-icons">access_time</Icon> <span style={styles.statLabel}>Est. Duration: {this.props.stats.duration}</span>
						</div>
						<div style={styles.modStat}>
							<Icon className="material-icons">timeline</Icon> <span style={styles.statLabel}> {this.props.stats.sections}</span>
						</div>
						<div style={styles.modStat}>
							<Icon className="material-icons">touch_app</Icon> <span style={styles.statLabel}>{this.props.stats.interactions}</span>
						</div>
						<div style={styles.modStat}>
							<Icon className="material-icons">movie</Icon> <span style={styles.statLabel}> {this.props.stats.videos}</span>
						</div>
					
				</div>
			</div>
		)	
	}
}
