var React = require('react')

import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';

import Drawer from 'material-ui/Drawer';
import Menu, { MenuItem } from 'material-ui/Menu';
import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';
import Divider from 'material-ui/Divider';
import List, {
	ListItem,
	ListItemAvatar,
	ListItemIcon,
	ListItemSecondaryAction,
	ListItemText,
} from 'material-ui/List';
import Typography from 'material-ui/Typography';
import * as CourseActions from '../../actions/CourseActions'
import CourseStore from '../../stores/CourseStore'

import Toolbar from 'material-ui/Toolbar'

const styles = {
	root: {
		width: '100%',
	},
	flex: {
		flex: 1,
	},
	menuButton: {
		marginLeft: -12,
		marginRight: 20,
	},
};

class Resources extends React.Component {
	constructor(){
		super()
		this.state = {
			resources: require('../../../data/resources.json'),
			open: false
		}
	}
	componentWillMount() {
		CourseStore.on( "show_resources", () => {this.setState({open: true })})
	}
	_closeResources = () => {
		this.setState({
			open: false
		})
	}
	goResource = (url) => {
		var win = window.open(url, '_blank');
		win.focus();
	}
	render() {
		let that = this
		const {resources, open} = this.state
		const styles = {
			drawer: {
				height: "auto",
				minHeight: "100vh",
				background: "#fff",
				width: "100%",
				maxWidth: "400px",
			},
			toolbar: {
				background: "#fff",
				boxShadow: "rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px",
				color: "#ccc",
				height: "48px",
			},
			toolbarTitle: {
				lineHeight: "48px",
				paddingLeft: "16px",
			},
		}
		let resourceEls = _.map(resources, function(resource, index){
			return [
			<ListItem button onTouchTap={ that.goResource.bind(this, resource.url) } target="_blank">
			<ListItemIcon>
			{ 
				resource.type == "pdf" ? 
				<Icon className="material-icons">picture_as_pdf</Icon>
				: resource.type == "website" ?
				<Icon className="material-icons">web</Icon>
				: resource.type == "doc" ?
				<Icon className="material-icons">library_books</Icon>
				: resource.type == "xls" ?
				<Icon className="material-icons">library_books</Icon>
				: 
				<Icon className="material-icons">library_books</Icon>
			}
			</ListItemIcon>
			<ListItemText
			primary={resource.title}
			/>
			</ListItem>, <Divider />

			]

		})
		return (
			<Drawer open={open} width={300}>
			<Toolbar>
			<Typography variant="title" color="inherit" style={{flex: 1}}>Resources</Typography>
			<IconButton onTouchTap={this._closeResources}>
			<Icon className="material-icons">close</Icon>
			</IconButton>
			</Toolbar>
			<List>{ resourceEls }</List>
			</Drawer>
			)              
	}
}

Resources.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Resources);
