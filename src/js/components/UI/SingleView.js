var React = require('react')

import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'

import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'

import Section from "./Section"

const styles = theme => ({
  	
})

class SingleView extends React.Component {
	constructor(props){
		super()
		_.map(props.data, (value, key) => {
			_.has(value, 'sectionId') ? this.state = { view: Object.assign({sectionId: value.sectionId}, props.data) } : null
		})		
	}

	render() {
		const { view } = this.state
		const styles = {
			cont: {
				paddingTop: 85,
			},
		}
		
		return (
			<div style={ styles.cont }>
				<Section { ...view }/>
			</div>
		)	
	}
}

export default withStyles(styles)(SingleView)
