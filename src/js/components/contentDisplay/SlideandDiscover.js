var React = require('react')
var $ = require('jquery')
import _ from 'lodash'
import ReactDOM from 'react-dom'

import Slider from 'material-ui-old/Slider'
import MuiThemeProvider from 'material-ui-old/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui-old/styles/getMuiTheme'

import ButtonBase from 'material-ui/ButtonBase'
import Icon from 'material-ui/Icon'
import IconButton from 'material-ui/IconButton'
import Paper from 'material-ui/Paper'
import ReactHtmlParser from 'react-html-parser'

export default class SlideandDiscover extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			width: 0,
			height: 0,
			sliderVal: .125,
			selectedItem: props.pairs[0],
		}
	}

	updateWindowDimensions = () => {
	  this.setState({ width: window.innerWidth, height: window.innerHeight })
	}

	componentDidMount = () => {
		this.updateWindowDimensions()
		window.addEventListener('resize', this.updateWindowDimensions)
	}

	componentWillUnmount() {
	  window.removeEventListener('resize', this.updateWindowDimensions);
	}

    onChange = (event, val) => {
    	const { pairs } = this.props
	    val < 1 && this.setState({ sliderVal: val, selectedItem: pairs[Math.floor(val/(1/pairs.length))] })
	}

	handleClick = (key) => {
    	const { pairs } = this.props
		const val = (1/pairs.length)*(key+1) - (1/pairs.length)/2
		val < 1 && this.setState({ sliderVal: val, selectedItem: pairs[Math.floor(val/(1/pairs.length))] })
	}

	render() {
		const{ title, directions, pairs, componentId, style } = this.props
		const { completed, values, update, width, height, sliderVal, selectedItem } = this.state
		const styles = {
			centeredContent: {
				width: "100%",
				textAlign: "center",
			},
			button: {
				margin: "20px 10px",
			},
			interactionCont:{
				transition: "all 500ms ease-in",
				minHeight: 550,
			},
			shadow: {
			    boxShadow: "0 0 100px rgba(128, 128, 128, 0.3) inset",
			    color: "#898989",
			    background: "rgb(236, 236, 236)",
			},
			inner: {
				minWidth: "100%",
			    color: "#898989",
		        margin: '0 auto',
			    boxShadow: "0 0 100px rgba(128, 128, 128, 0.3) inset",
			    background: "rgb(236, 236, 236)",
			    marginTop: 60,
			},
			interactiveIcon: {
				fontSize: "80px",
				color: "#898989",
			},
			defBoxContainer:{
				display: 'flex',
			},
			defBox: {
				position: "relative",
				width: '100%',
				maxWidth: "1200px",
				textAlign:"left",
				padding: "20px",
				margin: '20px auto',
				background: "#fff",
				border: '1px solid #002469',
				display: "inline-block",
				background: '#fff',
			},
			closeDefIcon: {
				position: "absolute",
				right: "10px",
				top: "10px",
			},
			defTitle: {
				marginTop: "0px",
				color: '#002469',
			},
			root: {
				display: 'inline-block',
			},
			gridList: {
				display: 'flex',
				flexWrap: 'nowrap',
				overflowX: 'auto',
			},
			titleStyle: {
				color: 'rgb(0, 188, 212)',
			},
			respImg: {
				cursor: "pointer",
				maxWidth: "100%",
				width: "auto",
			},
			icon: {
				color: "#fff",
			},
			title: {
				fontWeight: "bold",
				textShadow: "none",
			},
			labels: { 
				fontSize: 20,
				width: '100%',
				height: '100%',
				textAlign: 'center',
				color: '#fff',
				fontFamily: 'Agenda-Medium, Arial, san-serif',
			    position: 'absolute',
			    left: 0,
			    right: 0,
			    top: 0,
			    bottom: 0,
			    padding: 5,
			    alignItems: 'center',
			}
		}
		const muiTheme = getMuiTheme({ slider: { handleSize: width < 480 ? 40 : 30, trackSize: 5, handleSizeActive: 20, trackColor: '#fff', trackColorSelected: '#fff', handleFillColor: '#003fb7', selectionColor: '#0a5efd', rippleColor: '#003fb7',  handleColorZero: '#fff' } })

		const that = this
		const itemNames = _.map(pairs, (value, key) => {
			return <div class="range-label"><ButtonBase focusRipple style={ styles.labels } onTouchTap={ this.handleClick.bind(this, key) }></ButtonBase>{ value.item }</div>
		})

		let gridClass = "container mdl-grid"
		!this.props.columnSpacing ? gridClass += " mdl-grid--no-spacing" : null

		return (
			<div style={ title ? this.props.innerChild ? { ...styles.inner, ...style } : { ...styles.shadow, ...style } : style }>
				<div style={ styles.interactionCont } class={ gridClass } key={ componentId }>
					<div style={ styles.centeredContent }>
						{ title ? <h2 >{ title }</h2> : null }
						{ directions ? <h4 >{ directions }</h4> : null } 
						<div style={ { marginTop: 25 } }>
							<div style={ { margin: width < 480 ? '0 20px' : '0 35px', height: width < 480 ? 48 : 25, position: 'relative' } }><div class="range">{ itemNames }</div></div>
							<MuiThemeProvider muiTheme={muiTheme}>
								<div style={ { margin: width < 480 ? '0 20px' : '0 35px' } }><Slider value={ this.state.sliderVal } onChange={ this.onChange } /></div>
							</MuiThemeProvider>
							<div style={ styles.defBoxContainer }>
								<Paper style={ styles.defBox } key={ selectedItem.item }>
									<h3 style={ styles.defTitle }>{ selectedItem.item }</h3>
									<div class='defBoxContent' style={styles.defBoxContent}>
										{ ReactHtmlParser(selectedItem.def) }
									</div>
								</Paper>
							</div>
						</div>
					</div>
				</div>
			</div>
		)	
	}
}
