var React = require('react')
var $ = require('jquery')
var jQueryui = require('../../lib/jquery-ui-1.12.1/jquery-ui.min')
require('../../lib/jquery-ui-1.12.1/jquery-ui.min.css')
import _ from 'lodash'
import ReactDOM from 'react-dom'

import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import IconButton from 'material-ui/IconButton';
import Paper from 'material-ui/Paper';
import ReactHtmlParser from "react-html-parser"
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'

import * as CourseActions from '../../actions/CourseActions'
import LmsStore from '../../stores/LmsStore'
import MsgStore from '../../stores/MsgStore'
import ModalStore from '../../stores/ModalStore'

export default class DragNDrop extends React.Component {
	constructor(props){
		super(props)
		const savedState = LmsStore.returnInteractionData(props.componentId)
		//console.log(savedState)
		if(savedState != undefined){
			this.state = {
				selectedItemIndex: savedState.si,
				//selectedItem: props.pairs[savedState.si],
				completed: savedState.c,
				//svg: props.type == 'svg' ? <object id="svg-items" type="image/svg+xml" data={ this.props.svgUrl } /> : null
			}
		}else{
			//console.log(props)
			this.state = {
				//completed: Array(props.pairs.length).fill(0),
				//svg: props.type == 'svg' ? <object id="svg-items" type="image/svg+xml" data={ this.props.svgUrl } /> : null
			}
		}
	}

	componentDidMount() {
		this.initDrag()
	}

	initDrag(){
		let that = this
		$('.drag').draggable({ 
			containment: "#scrollTemp", 
			revert: "invalid", 
			revertDuration: 600, 
			start: function(){
	        	$(this).css('z-index', '1');
	        	$(this).removeClass('dropped');
	        	$(this).parent().parent().data('filled', 0);
	        	$(this).find('.wrongIndicator').fadeOut('slow');
	        },
	        stop: function(){
	        	if($(this).parent().attr('id') != 'dragHolder'){
	        		$(this).addClass('dropped');
	        		$(this).parent().data('filled', 1);
	        	}
	        },
	    });

		$('.drop').droppable({accept: '.drag', 
			//hoverClass: "drop-hover",
	        drop: function( event, ui ) {
	        	if(!that.isFilled(this)){
	        		ui.draggable.draggable( "option", "revert", true );
	        	}else{
	        		that.placeDrag(ui.draggable, this);
		            that.checkComplete();
	       		}
	        }
	    });
	}

	 isFilled(drop){
		if($(drop).data( "filled") == 1){
			return false;
		}else{
			$(drop).data( "filled", 1);
			return true;
		}
	}

	placeDrag(drag, drop){
		$(drop).data( "filled", 1);
		$(drop).find('.targetImg').prepend(drag);
		$(drag).css({
			display: "block",
			positon: "absolute",
			left: 0,
			bottom: 0,
			top: "42px",
			padding: 0,
		});
		$(drop).find('.targetImg').css({
			marginTop: "0px",
		});
	}


	 checkComplete(){
		let numComplete = 0;
		$('.drop').each(function(){
			if($(this).data('filled') == 1){
				numComplete++
			}
		})
		console.log(numComplete,  this.props.items.length)
		if(numComplete >= this.props.items.length){
			console.log("COMPLETE")
			$('.chkAnswer').fadeIn();
			$('.chkAnswer').bind('click', function(){
				$('.chkAnswer').fadeOut();
				submitAnswer();
			})
		}else{
			$('.sbmtBtn').fadeOut();
		}
	}

	submitAnswer(){
		numTrys--;
		incorrectAry = [];
		$('.drop').each(function(index){
			dropID = $(this).attr('id').split('_')[1];
			dragID = $(this).find('.drag').attr('id').split('_')[1];
			if(dropID != dragID){
				incorrectAry.push(index+1);
			}
		})
		$.each(incorrectAry, function( index, value ) {
		  incorrectItem = $('#drag_'+value)[0];
		  $(incorrectItem).removeClass('dropped');
		  $(incorrectItem).parent().data('filled', 0);
		  $(incorrectItem).css({'top': '0px', 'left':'0px'})
		  $('#dragHolder').append(incorrectItem);
		  $(incorrectItem).find('.dragChild').append('<div class="wrongIndicator"><i class="fa fa-times wrong"></i></div>')
		});
		if(incorrectAry.length > 0){
			if(numTrys > 0){
				$('.feedback').text('Sorry, that is not correct. Please try again.');
			}else{
				$('.feedback').text('Sorry, that is not correct. Please try again.');
			}
		}else{
			$('.feedback').html('Good work! That’s the correct sequence! <br><br> Consider how the students were set up for success from the beginning of the lesson. Also note that scaffolding teaching should be used throughout the lesson, from the beginning to the end. ');
		}
		$('.feedback').show();

	}
	
	
	render() {
		const{ title, directions, pairs, type, componentId } = this.props
		const { selectedItem, completed, svg } = this.state
		const styles = {
			centeredContent: {
				width: "100%",
				textAlign: "center",
			},
			button: {
				margin: "20px 10px",
			},
			interactionCont:{
				transition: "all 500ms ease-in",
				padding: "60px 0",
			},
			shadow: {
			    boxShadow: "0 0 100px rgba(128, 128, 128, 0.3) inset",
			    color: "#898989",
			    background: "rgb(236, 236, 236)",
			},
			dragItem: {
				display: "inline"
			},
			dragImg: {
				width: "auto",
				maxWidth: "150px",
			},
			targetImg: {
				width: "auto",
				maxWidth: "150px",
			},
			target: {
				display: "flex",
				flexDirection: "column",
				height: "100%",
			},
			targetDesc: {
				flex: 4,
				margin: "5px",
    			padding: "13px",
			},
			targetImg: {
				flex: 1,
				marginBottom: "20px",
				marginTop: "79px",
			},
			targetImgEl: {
				maxWidth: "150px",
				width: "auto",
			},
			title: {
				fontFamily: this.props.muiTheme.fontFamily
			},
			/*inner: {
				minWidth: "100%",
			    color: "#898989",
		        margin: '0 auto',
			    boxShadow: "0 0 100px rgba(128, 128, 128, 0.3) inset",
			    background: "rgb(236, 236, 236)",
			    marginTop: 60,
			},
			interactiveIcon: {
				fontSize: "80px",
				color: "#898989",
			},
			defBox: {
				position: "relative",
				maxWidth: "1200px",
				textAlign:"left",
				padding: "20px",
				margin: "auto",
				background: "#fff",
			},
			svgBoxCont: {
				padding: "0 5% 0 0",
			},
			svgDefBox: {
				position: "relative",
				maxWidth: "1200px",
				textAlign:"left",
				padding: "20px",
				margin: "auto",
				background: "#fff",
				marginTop: "20%",
			},
			closeDefIcon: {
				position: "absolute",
				right: "10px",
				top: "10px",
			},
			defTitle: {
				marginTop: "0px",
			}*/
		}

		const that = this
		/*let children = []
		switch(type){
			case 'terms' :
				children = _.map(pairs, (value, key) => {
					return <Button key={key} label={value.item} onTouchTap={that.selectItem.bind(that, key)} primary={true} style={styles.button} icon={completed[key] == 1 ? <ActionCheckCircle /> : null} ></Button>
				})
			break
			case 'images' :
				children = _.map(pairs, (value, key) => {
					return <Button key={key} label={value.item} onTouchTap={that.selectItem.bind(that, key)} primary={true} style={styles.button} icon={completed[key] == 1 ? <ActionCheckCircle /> : null} ></Button>// -- img tags
				})
			break
			case 'launchable':
				children = _.map(pairs, (value, key) => {
					return <Button key={key} label={value.title} onTouchTap={that.selectItem.bind(that, key)} primary={true} style={styles.button} icon={completed[key] == 1 ? <ActionCheckCircle /> : null} ></Button>
				})
			break
			case 'svg':

			break
		}*/

		/*const defId = selectedItem ? selectedItem.item.replace(/\W+/g, '-').toLowerCase() : null
		const defBox = selectedItem && type != "launchable" ? <div id={defId}>
										<Paper style={type == 'svg' ? styles.svgDefBox : styles.defBox} key={selectedItem.item}>
											<IconButton  onTouchTap={this.closeDef} style={styles.closeDefIcon} tooltip="Close">
									     		<Icon className="material-icons" >close</Icon>
										    </IconButton>
											<h3 style={styles.defTitle}>{type == 'svg' ? selectedItem.title : selectedItem.item}</h3>
											<div style={styles.defBoxContent}>
												{ ReactHtmlParser(selectedItem.def) }
											</div>
										</Paper>
									</div> : null*/ 

		let gridClass = "container mdl-grid"
		!this.props.columnSpacing ? gridClass += " mdl-grid--no-spacing" : null

		let dragItems = _.map(this.props.items, function(item, key){
			return <div id={ "drag_"+key } class='draggable drag' style={styles.dragItem}><img style={ styles.dragImg } src={ item.url }/> </div>
		})
		let targets = _.map(this.props.targets, function(target, key){
			return <div class="mdl-cell mdl-cell--4-col mdl-cell--stretch"><div id={ "drop_"+key } class='droppable drop' style={styles.target}><div class="targetImg" style={styles.targetImg} ><img style={ styles.targetImgEl } src={ target.url }/></div><Paper style={styles.targetDesc} >{ target.desc }</Paper>  </div></div>
		})
		return (
			<div style={ title ? this.props.innerChild ? styles.inner : styles.shadow : null }>
				<div style={ styles.interactionCont } class={ gridClass } key={ componentId }>
					<div style={ styles.centeredContent }>
						{ title ? <div class="golfBall"><Icon className="material-icons" >golf_course</Icon></div> : null }
						{ title ? <h2 style={this.props.muiTheme.titles}>{title}</h2> : null }
						{ directions ? <h4 style={this.props.muiTheme.subTitles}>{directions}</h4> : null }
						<div class={gridClass}>
							<div class="mdl-cell mdl-cell--12-col" >
								<div id='dragHolder'>
									{ dragItems }
								</div>
							</div>
							{ targets }
						</div>
					</div>
				</div>
			</div>
		)	
	}
}
