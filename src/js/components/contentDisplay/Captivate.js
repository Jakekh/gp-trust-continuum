var React = require('react')
var $ = require('jquery')

export default class Captivate extends React.Component {
	constructor(props){
		super(props)
			this.state = {
				captivateHeight: null
			}
	}
	calcSize = () => {
		const { source } = this.props
		let calcHeight = (  source.width /source.height ) * $('.container').width() 
		this.setState({
			captivateHeight: calcHeight.toFixed(),
			captivateWidth: $('.container').width()
		})
	}

	componentDidMount() {
		this.calcSize()
		let that = this
		//$(window).on("resize", this.calcSize)
	}
	
	render() {
		const{ source } = this.props
		const { captivateHeight, captivateWidth } = this.state
		const styles = {
			captivateFrame: {
				height: `${captivateHeight}px`,
				width: "100%"

			}
		}

		return (
			<div class="shadowSection">
				<div class="container">
					<div>
						 <iframe height={captivateHeight} width={captivateWidth} frameBorder="0" style={styles.captivateFrame} src={source.url} ></iframe>
					</div>
				</div>
			</div>
		)	
	}
}
