var React = require('react')

import ReactHtmlParser from "react-html-parser"

import ButtonBase from 'material-ui/ButtonBase'
import Icon from 'material-ui/Icon';
import InformationalPane from "./InformationalPane"
import ClickandDiscover from "./ClickandDiscover"
import SlideandDiscover from "./SlideandDiscover"
import Question from "./Question"
import InteractionBumper from '../UI/InteractionBumper'
import * as CourseActions from '../../actions/CourseActions'
import LmsStore from '../../stores/LmsStore'
import ModalStore from '../../stores/ModalStore'
import { darken } from 'material-ui-old/utils/colorManipulator'

import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'

const styles = theme => ({
	button: {
	    position: 'relative',
        display: 'block',
	    width: '100%',
	    height: '100%',
	    '&:hover': {
	      	zIndex: 1,
	    },
	    '&:hover $imageBackdrop': {
	      	opacity: 0.15,
	    },
	},
	imageBackdrop: {
	    position: 'absolute',
	    left: 0,
	    right: 0,
	    top: 0,
	    bottom: 0,
	    backgroundColor: theme.palette.common.black,
	    opacity: .3,
	    transition: theme.transitions.create('opacity'),
	},
})

class CaseStudies extends React.Component {
	constructor(props){
		super(props)
		const savedState = LmsStore.returnInteractionData(props.componentId)
		if(savedState != undefined){
			this.state = {
				caseStudieEls: null,
				completed: savedState.c,
				width: 0,
				height: 0
			}
		}else{
			this.state = {
				caseStudieEls: null,
				completed: Array(props.caseStudies.length).fill(0),
				width: 0,
				height: 0
			}
		}
		this.gridClass = ''
	}

	updateWindowDimensions = () => {
	  this.setState({ width: window.innerWidth, height: window.innerHeight })
	}

	componentDidMount = () => {
		this.updateWindowDimensions()
		window.addEventListener('resize', this.updateWindowDimensions)
	}

	componentWillUnmount() {
	  window.removeEventListener('resize', this.updateWindowDimensions);
	}

	buildColumn = (props, numColumns, key) => {
		const { classes } = this.props
		const styles = {
			completeIconHolder: {
				width: "100%",
				textAlign: "center",
				padding: "20px 0",
			},
			completeIcon: {
				color: "#fff",
				fontSize: "30px",
			},
			caseStudyItem: {
				padding: "15px",
			}
		}
		this.gridClass = 'mdl-cell mdl-cell--12-col'
		if(props.forceCol){ 
			this.gridClass = 'mdl-cell mdl-cell--'+props.forceCol.desktop+'-col mdl-cell--'+props.forceCol.tablet+'-col-tablet mdl-cell--'+props.forceCol.tablet+'-col-phone'
		}else{
			switch(numColumns){
				case 1:
					this.gridClass = 'mdl-cell mdl-cell--12-col'
				break
				case 2:
					this.gridClass = 'mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet'
				break
				case 3:
					this.gridClass = 'mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet'
				break
				case 4:
					this.gridClass = 'mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet'
				break
			}
		}
		return <div class={ this.gridClass } key={"col_"+ key} style={ styles.caseStudyItem }>
			<ButtonBase focusRipple class={ classes.button } onTouchTap={ this.showCaseStudy.bind(this, key) }>
				<span class={ classes.imageBackdrop } />
				<div class="img-title" >
					{ props.title } 
					{ this.state.completed[key] == 1 ? 
						<div style={ styles.completeIconHolder }><Icon style={ styles.completeIcon }>check_circle</Icon></div>
				 	: null }
				 </div>
				<img className="img-responsive" src={ props.img } />
			</ButtonBase>
		</div>
	}
	showCaseStudy = (caseNum) => {
		let that = this
		const { caseStudies, componentId } = this.props
		const components = {
		    InformationalPane: InformationalPane,
		    ClickandDiscover: ClickandDiscover,
		    SlideandDiscover: SlideandDiscover,
		    Question: Question,
		};
		
		let childrenIndex = 0
		const sectionComponents = _.map( caseStudies[caseNum].content, function(value, childKey){
			childrenIndex ++
			let subComps =_.map( value, function(component, compKey){
				const ComponentType = components[compKey]
				if( component.addContainer ){
					return <div key={ `${compKey}_${childrenIndex}_${caseNum}_container` } class="container"><ComponentType muiTheme={that.props.muiTheme} { ...component } 
	    				threshold={that.props.threshold ? that.props.threshold : null} 
	    				key={ `${compKey}_${childrenIndex}_${caseNum}` }
	    				componentId={`${componentId}_${childKey}_${caseNum}`}/></div>
				}else{
	    			return <ComponentType muiTheme={that.props.muiTheme} { ...component } 
	    				threshold={that.props.threshold ? that.props.threshold : null} 
	    				key={ `${compKey}_${childrenIndex}_${caseNum}` }
	    				componentId={`${componentId}_${childKey}_${caseNum}`}/>
   				}
	    	})
			 return subComps
		})
		let newAry = this.state.completed
		newAry[caseNum] = 1

		newAry.indexOf(0) == -1 ? 
			that.props.getNextThreshold ? that.props.getNextThreshold() : null
		: null
		that.setState({
			//caseStudieEls : sectionComponents,
			completed: newAry,
		})

		sectionComponents.push(<InteractionBumper key={`InteractionBumper_${caseNum}`} style={{background: "#fff", color: "#002469", position: 'relative', bottom: 0, left: 0}} text={caseStudies[caseNum].directions} />)

		ModalStore.createModal(sectionComponents, {backgroundColor: darken(caseStudies[caseNum].color, .3 ), backgroundImage: `url(${caseStudies[caseNum].bg})` })
		CourseActions.saveInteraction({id: that.props.componentId, c: that.state.completed})
	}
	render() {
		const{ title, caseStudies, directions, classes } = this.props
		const { caseStudieEls, completed, width, height } = this.state

		let numColumns = caseStudies.length
		let that = this
		let children = _.map( caseStudies, function(value, key){
			return that.buildColumn(value, numColumns, key)
		})

		const styles = {
			centeredContent: {
				width: "100%",
				textAlign: "center",
			},
			chip: {
				borderRadius: '20px',
			    height: '40px',
			    transition: 'all .5s'
			},
			infoPane: {
				background: this.props.background ? "url("+ this.props.background.url + ")" : "transparent",
				backgroundSize: "cover",
				position: "relative",
				borderBottom: this.props.background ? "50px solid #fff" : "none",
			},
			directions: {
				textAlign: "left",
				maxWidth: '800px',
				margin: "15px auto",
				marginTop: "0px",
			},
			mobileDirections: {
			    color: '#002469',
			    fontSize: 26,
			    lineHeight: '27px',
			    fontWeight: 400,
			    padding: 15,
			},
		}

		let gridClass = "container mdl-grid"
		!this.props.columnSpacing ? gridClass += " mdl-grid--no-spacing" : null
		return (
			<div>
				<div class={this.props.shadowed ? "shadowSection" : null}>
					<div style={styles.infoPane} class={gridClass} key={title}>
						{ this.props.icon || title || directions ?
							<div style={styles.centeredContent}>
								{ this.props.icon ? <div class="golfBall"><Icon className="material-icons" >{this.props.icon}</Icon></div> : null }
								{ title && <h2>title</h2> }
								{ directions && <h4>directions</h4> }
							</div>
						: null }
						{ width < 840 && this.props.mobileDirections && <div class={ this.gridClass } style={ styles.mobileDirections }>{ ReactHtmlParser(this.props.mobileDirections) }</div> }
						{ children }
					</div>
				</div>
			</div>
		)	
	}
}

CaseStudies.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles, { withTheme: true })(CaseStudies)