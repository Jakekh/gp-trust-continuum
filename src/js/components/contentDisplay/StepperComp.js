import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'
import MobileStepper from 'material-ui/MobileStepper'
import Button from 'material-ui/Button'
import Typography from 'material-ui/Typography'
import InformationalPane from "./InformationalPane"

import Stepper, { Step, StepLabel, StepContent, StepButton } from 'material-ui/Stepper'

const styles = theme => ({
  root: {
    background: "transparent"
  },
  label: {
  	fontSize: "25px",
  },
  iconContainer: {
  	fontSize: ".9rem",
  },
  stepButton: {
    justifyContent: "flex-start",
    textAlign:"left",
  }
})

class StepperComp extends React.Component {

  state = {
    activeStep: 0,
    completed: {0: true},
    complete: false,
    stepContent: []
  }

  completedSteps() {
    const { completed } = this.state
    return Object.keys(completed).length
  }

  isLastStep() {
    const { steps } = this.props
    return this.state.activeStep === steps.length - 1
  }

  allStepsCompleted() {
    const { steps } = this.props
    return this.completedSteps() === steps.length
  }

  handleNext = () => {
    const { steps } = this.props
    const { completed } = this.state
    let activeStep

    if(this.isLastStep() && !this.allStepsCompleted()) {
      activeStep = steps.findIndex((step, i) => !(i in completed))
    } else {
      activeStep = this.state.activeStep + 1
    }

    this.setState({ activeStep }, () => {
      this.checkComplete()
    })
  }

  handleBack = () => {
    const { activeStep } = this.state
    this.setState({ activeStep: activeStep - 1 }, () => {
      this.checkComplete()
    })
  }

  handleStep = step => () => {
    this.setState({ activeStep: step }, () => {
      this.checkComplete()
    })
  }

  checkComplete = () => {
    const { completed } = this.state

    completed[this.state.activeStep] = true
    this.setState({ completed }, () => { this.allStepsCompleted() && this.setComplete() })
  }  

  setComplete = () => {
    const { getNextThreshold } = this.props
    !this.state.complete && getNextThreshold && getNextThreshold()
    this.setState({ complete: true })
  }
  componentDidMount = () => {
    this.buildSteps()
  }
	buildSteps =() => {
    const { style, classes, theme, steps } = this.props
    const { activeStep } = this.state

    let allStepEls =  steps.map((step, index) => {
      return <InformationalPane { ...step.InformationalPane } />
    })
    this.setState({
      stepContent: allStepEls 
    })
  }
  render() {
  	let that = this
  	const styles = {
  		nav: {
  			background: "transparent",
  		},
  		wbt_background: {
  			position: "relative",
  			overflow: "hidden",
  		},
  		panelHolder: {
				position: 'absolute',
				top: 0,
				width: "100%",
  		}
  	}
    const { style, classes, theme, steps } = this.props
    const { activeStep, stepContent } = this.state

    return (
      <div style={style}>
      	<div class="container">
  				<Stepper className={ classes.root } activeStep={ activeStep } orientation="vertical" nonLinear>
            {steps.map((step, index) => {
              return (
                <Step key={"step_"+index}>
                  <StepButton className={ classes.stepButton } 
                    onClick={this.handleStep(index)}
                    completed={this.state.completed[index]}><div style={{ fontSize: "25px" }}>{ step.label }</div></StepButton> 
                  <StepContent>
                   { stepContent[activeStep] }
                    <div className={classes.actionsContainer}>
                      <div>
                        <Button
                          disabled={activeStep === 0}
                          onClick={this.handleBack.bind(that)}
                          className={classes.button}
                        >
                          Back
                        </Button>
                        <Button
                          variant="raised"
                          color="primary"
                          onClick={this.handleNext.bind(that)}
                          className={classes.button}
                        >
                          {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                        </Button>
                      </div>
                    </div>
                  </StepContent>
                </Step>
              );
            })}
          </Stepper>
        </div>
      </div>
    )
  }
}

StepperComp.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
}

export default withStyles(styles, { withTheme: true })(StepperComp)