var React = require('react')
var $ = require('jquery')

import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import Paper from 'material-ui/Paper';
import ReactHtmlParser from "react-html-parser"
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'
import InteractionBumper from '../UI/InteractionBumper'
import update from 'immutability-helper';
import Card, { CardHeader, CardActions, CardContent } from 'material-ui/Card';
import * as CourseActions from '../../actions/CourseActions'
import LmsStore from '../../stores/LmsStore'
import CourseStore from '../../stores/CourseStore'
import MsgStore from '../../stores/MsgStore'

import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';

export default class AssessmentReview extends React.Component {
	
	buildReviewTable = (data) => {
		let that = this
		let styles = {
			cell: {
				overflow: "hidden",
		    	whiteSpace: "initial",
		    	textOverflow: "initial",
	    	    wordWrap: "break-word",
	    	    padding: "10px 24px",
			},
			width_20: {
				width: "20%"
			},
			buttoncell:{
				width: "80px"
			}
		}
		let table = 
		<Table>
        <TableHead>
          <TableRow>
            <TableCell style={styles.width_20}>Question ID</TableCell>
            <TableCell>Question Text</TableCell>
            <TableCell>Your Answer</TableCell>
            <TableCell style={{...styles.cell, ...styles.buttoncell}}>&nbsp;</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        { 
	       	_.map(data, function(question, index){
	       		return <TableRow key={`assRow_${index}`}>
			            <TableCell style={{...styles.cell, ...styles.width_20}}>{question.id}</TableCell>
			            <TableCell style={styles.cell}>{question.description}</TableCell>
			            <TableCell style={styles.cell}>{question.learner_response}</TableCell>
			            <TableCell style={{...styles.cell, ...styles.buttoncell}}><Button onTouchTap={that.props.reviewQuestion.bind(that, question, index)}>Review</Button></TableCell>
			          </TableRow>
	        })
   		}
        </TableBody>
      </Table>
      return table
	}

	render() {
		const {scormInts} = this.props
		let that = this
		const styles = {
			
		}
		let table = this.buildReviewTable(scormInts)
		return (
			<Card style={{width: "100%"}}>
			<CardHeader
            title="Assessment Review"
            subheader="Before submitting your answers, take a minute to review them."
          />
        <CardContent>
  				<div>{ table }</div>
        </CardContent>
        <CardActions>
          <Button color={"primary"} variant={"raised"} onTouchTap={this.props.completeAssessment.bind(this)}>Submit Assessment</Button>
        </CardActions>
      </Card>
		)	
	}
}
