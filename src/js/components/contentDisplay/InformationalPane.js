var React = require('react')
var $ = require('jquery');

import ReactHtmlParser from "react-html-parser"
import 'animation.gsap'
import 'debug.addIndicators'
import 'TextPlugin'
var ScrollMagic = require('scrollmagic')
import {TweenMax, TimelineLite, Animation} from "gsap"

import AudioPlayer from "../UI/AudioPlayer"
import VideoPlayer from "../UI/VideoPlayer"
import SpeechBubble from "../UI/SpeechBubble"
import CardComp from "./CardComp"

export default class InformationalPane extends React.Component {
	constructor(){
		super()
		this.state = {
			//view: CourseStore.returnCourseView(),
		}
	}
	buildContent = (value, elIndex, colIndex, iteIndex) => {
		let that = this
		let el = null
		const relativeStyle = {
			position: "relative"
		}
		const circledStyle = {
			margin: "0 auto",
			width: "100%",
			maxWidth: "300px",
			maxHeight: "300px",
			borderRadius: "300px",
			overflow: "hidden",
		   	background: "#6c3",
	    	boxShadow: "0px 3px 7px rgba(0,0,0,.2)",
		}
		value.type == "SpeechBubble" ?
			el = <SpeechBubble { ...value } key={ "speechBubble_"+ elIndex} />
		:
		value.link ?
			el = <a href={value.link} target="_blank" key={"content_"+elIndex+"_"+iteIndex} ><div class="section-content-text" >{ ReactHtmlParser(value.text) }</div></a>
		:
		value.type == "AccentBox" ?
			el = <div class="acccent-box-parent" key={"accent_"+elIndex+"_"+iteIndex}><div class="acccent-box-child">{ ReactHtmlParser(value.text) }</div></div>
		:
		value.type == "text" ?
			el = <div class="section-content-text" style={ value.style ? value.style : null } key={"content_"+elIndex+"_"+iteIndex} id={`${value.type}_${that.props.componentId}_${colIndex}`}>{ ReactHtmlParser(value.text) }</div>
		:
		value.type == "card" ?
			el = <CardComp data={ value } key={"content_"+elIndex+"_"+iteIndex} id={`${value.type}_${that.props.componentId}_${colIndex}`} />
		:
		value.type == "image" ?
			value.link ?
				el = <div id={`${value.type}_${that.props.componentId}_${colIndex}`} style={ value.style ? value.style : value.circled ? circledStyle : relativeStyle} key={"content_"+elIndex+"_"+iteIndex}><a href={value.link} target="_blank" ><img key={"img_"+key} style={ value.imgStyle ? value.imgStyle : null} src={value.url} class="img-responsive" /></a><br /><span>{value.caption}</span></div>
			:
				el = <div id={`${value.type}_${that.props.componentId}_${colIndex}`} style={ value.style ? value.style : value.circled ? circledStyle : relativeStyle} key={`${value.type}_${elIndex}_${iteIndex}`}><img style={ value.imgStyle ? value.imgStyle : null} src={value.url} class="img-responsive" /><br /><span>{value.caption}</span></div>
		:
		value.type == "svg" ?
			value.link ?
				el = <div id={`${value.type}_${that.props.componentId}_${colIndex}`} style={ value.style ? value.style : value.circled ? circledStyle : relativeStyle} key={"content_"+elIndex+"_"+iteIndex}><a href={value.link} target="_blank" ><object key={"img_"+key} style={ value.imgStyle ? value.imgStyle : null} data={value.url} class="img-responsive"></object></a><br /><span>{value.caption}</span></div>
			:
				el = <div id={`${value.type}_${that.props.componentId}_${colIndex}`} style={ value.style ? value.style : value.circled ? circledStyle : relativeStyle} key={`${value.type}_${elIndex}_${iteIndex}`}><object style={ value.imgStyle ? value.imgStyle : null} data={value.url} class="img-responsive"></object><br /><span>{value.caption}</span></div>
		: 
		value.type == "video" ?

			el = <div key={"video_"+elIndex+"_"+iteIndex}>{ value.title ? <h3 style={ value.title.style ? value.title.style : null }>{ ReactHtmlParser(value.title.text) }</h3> : null } <VideoPlayer key={"video_"+elIndex} componentId={`${that.props.componentId}_${elIndex}`} data={value} /> { value.description ? <div class="section-content-text" style={ value.description.style ? value.description.style : null }>{ ReactHtmlParser(value.description.text) }</div> : null }</div>
		:
		value.type == "audio" ?
			value.quote ?
				el = <div class="quote-cont" key={"audio_quote_"+elIndex+"_"+iteIndex}><blockquote class="quote">{ ReactHtmlParser(value.quote.text) }</blockquote><p>{ ReactHtmlParser(value.quote.caption) }</p><br /><AudioPlayer data={value} /></div>
			:							
				el = <AudioPlayer key={"audio_"+elIndex} data={value} />
		
		: 
		value.type == "quote" ?
			el = <div class="quote-cont" key={"quote_"+elIndex+"_"+iteIndex}><blockquote class="quote">{ ReactHtmlParser(value.text) }</blockquote><p>{ ReactHtmlParser(value.caption) }</p><br /></div>
		
		: null
		return el
	}
	buildColumn = (props, numColumns, key) => {
		let that = this
		let colIndex = key
		let gridClass = 'mdl-cell mdl-cell--12-col'
		if(props.forceCol){ 
			let offset = ""
			if(props.forceCol.offset){
				offset = "mdl-cell--"+props.forceCol.offset+"-offset-desktop"
			}
			gridClass = 'mdl-cell mdl-cell--'+props.forceCol.desktop+'-col mdl-cell--'+props.forceCol.tablet+'-col-tablet mdl-cell--'+props.forceCol.tablet+'-col-phone ' + offset
			props.forceCol.mobileOrder ? gridClass += " mdl-cell--order-"+props.forceCol.mobileOrder+"-phone" : null
		}else{
			switch(numColumns){
				case 1:
					gridClass = 'mdl-cell mdl-cell--12-col'
				break
				case 2:
					gridClass = 'mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet'
				break
				case 3:
					gridClass = 'mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet'
				break
			}
		}
		let elIndex = 0
		let columnEls = _.map(props, function(value, key) {
			//console.log("KEY", key, value)
			let el = []
			switch(key){
				case "content" :
					if(value.length && value.length > 1){
						//console.log("multiContent", value)
						el = _.map(value, function(item, itemIndex){
							//console.log("IN LOOP:", item, el)
							return that.buildContent(item, elIndex, colIndex, itemIndex )
						})
					}else{
						el = that.buildContent(value, elIndex, colIndex, 0)
					}
					return el
				break
				case "media" :
					

					return el
				break
			}
			elIndex++
		})
		//console.log("ELEMENTS:", columnEls, typeof(columnEls) )
		return <div class={gridClass} key={"col_"+ key}>{ _.map(columnEls, function(val){return val}) }</div>
	}
	componentDidMount() {
		const{ columns, title, animated } = this.props
		//console.log("TITLE: ", title)
		let that = this
		if(animated){
			var controller = new ScrollMagic.Controller({loglevel: 1, addIndicators: true})
			var anim_timeline_static = new TimelineMax();
			var anim_timeline_scroll = new TimelineMax();
			var anim_scene_static = new ScrollMagic.Scene({
				triggerElement: `#${that.props.parentId}`, 
				triggerHook: 0.5,
				duration: 0,
				reverse: false
			})
			var anim_scene_scroll = new ScrollMagic.Scene({
				triggerElement: `#${that.props.parentId}`, 
				triggerHook: 0.5,
				duration: 1000,
			})
			 _.map( columns, function(col, key){
					_.map(col, function(el, elKey){
						if(el.animation){
							if(el.animation.stagger){
								$(`#${el.type}_${that.props.componentId}_${key} ${el.animation.stagger.el}`).css("position", "relative")
								if(el.animation.type == "scroll"){
									if(el.animation.toFrom == "from"){
										anim_timeline_scroll.staggerFrom(	$(`#${el.type}_${that.props.componentId}_${key} ${el.animation.stagger.el}`) , el.animation.duration, el.animation.properties,  el.animation.stagger.duration)
									}else{
										anim_timeline_scroll.staggerTo(	$(`#${el.type}_${that.props.componentId}_${key} ${el.animation.stagger.el}`) , el.animation.duration, el.animation.properties,  el.animation.stagger.duration)
									}
								}else{
									if(el.animation.toFrom == "from"){
										anim_timeline_static.staggerFrom(	$(`#${el.type}_${that.props.componentId}_${key} ${el.animation.stagger.el}`) , el.animation.duration, el.animation.properties,  el.animation.stagger.duration)
									}else{
										anim_timeline_static.staggerTo(	$(`#${el.type}_${that.props.componentId}_${key} ${el.animation.stagger.el}`) , el.animation.duration, el.animation.properties,  el.animation.stagger.duration)
									}
								}
							}else{
								if(el.animation.type == "scroll"){
									if(el.animation.toFrom == "from"){
										anim_timeline_scroll.from(`#${el.type}_${that.props.componentId}_${key}`, el.animation.duration, el.animation.properties, el.animation.position)
									}else{
										anim_timeline_scroll.to(`#${el.type}_${that.props.componentId}_${key}`, el.animation.duration, el.animation.properties, el.animation.position)
									}
								}else{
									if(el.animation.toFrom == "from"){
										anim_timeline_static.from(`#${el.type}_${that.props.componentId}_${key}`, el.animation.duration, el.animation.properties, el.animation.position)
									}else{
										anim_timeline_static.to(`#${el.type}_${that.props.componentId}_${key}`, el.animation.duration, el.animation.properties, el.animation.position)
									}
								}
							}
						}
					})
					 
			})
		 	anim_scene_static.addTo(controller)
		 	anim_scene_scroll.addTo(controller)
			anim_scene_static.setTween(anim_timeline_static)
			anim_scene_scroll.setTween(anim_timeline_scroll)
		}
	}
	render() {
		const{ title, columns, innerChild, background, style, forceCol, columnSpacing, textBlock, quoteBlock, bottomCaret } = this.props

		let numColumns = columns.length
		let that = this
		let children = _.map( columns, function(value, key){
			return that.buildColumn(value, numColumns, key)
		})

		const styles = {
			infoPane: {
				background: background ? `url(${background.url})` : "transparent",
				backgroundSize: "cover",
				position: "relative",
				minHeight: background ? background.minHeight ? background.minHeight : "none" : "none",
				margin: innerChild && background ? "8px" : "0 auto",
				textAlign: background ? background.textCenter ? "center" : "left" : "left",
				paddingTop: background ? background.textCenter ? "60px" : "0px" : "0px",
				color: innerChild && !forceCol && background ? "#fff" : "#333",
				maxWidth: textBlock || quoteBlock ? '800px' : "auto",
				marginBottom: background ? "50px" : bottomCaret ? "60px" : "none",
			},
			innerBanner: {
				marginTop: "60px",
			},
			shadowed: {
				minWidth: "100%",
			    color: "#898989",
		        margin: '0 auto',
			    // boxShadow: "0 0 100px rgba(128, 128, 128, 0.3) inset",
			    // background: "rgb(236, 236, 236)",
			    margin: "30px 0px",
			}
		}

		let gridClass = innerChild ? "mdl-grid" : "container mdl-grid"
		!columnSpacing ? gridClass += " mdl-grid--no-spacing" : null
		return (
			<div style={ innerChild && quoteBlock ? {...styles.innerBanner, ...styles.shadowed} : innerChild && !quoteBlock ? styles.innerBanner : !innerChild && quoteBlock ? styles.shadowed : null } class="fullWidth" key={ "infoPane_"+Math.floor((Math.random() * 1000) + 1)  } >
				<div style={{...styles.infoPane, ...style}} class={gridClass} >
					{ textBlock && <h2>{ this.props.title }</h2> }
					{ children }
					{ bottomCaret && <div class="section-caret"></div> }
				</div>
			</div>
		)	
	}
}