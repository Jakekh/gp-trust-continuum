import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'
import MobileStepper from 'material-ui/MobileStepper'
import Button from 'material-ui/Button'
import Icon from 'material-ui/Icon'
import Section from '../UI/Section'

import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'
const styles = {
  
}

class Carousel extends React.Component {
  state = {
    activeStep: 0,
    complete: false
  }

  handleNext = () => {
    this.setState({ activeStep: this.state.activeStep + 1, next: true, }, () => {
    	this.checkComplete()
    })
  }

  handleBack = () => {
    this.setState({ activeStep: this.state.activeStep - 1, next: false, })
  }

  checkComplete = () => {
  	const { activeStep } = this.state
  	const { panels } = this.props
  	activeStep == panels.length - 1 && this.setComplete()
  }

  setComplete = () => {
  	const { getNextThreshold } = this.props
  	!this.state.complete && getNextThreshold && getNextThreshold()
  	this.setState({ complete: true })
  }

  render() {
  	const styles = {
  		nav: {
  			background: "#fff",
  		},
  		wbt_background: {
  			position: "relative",
  			overflow: "hidden",
  		},
  		panelHolder: {
				position: 'absolute',
				top: 0,
				width: "100%",
  		}
  	}
    const { classes, theme, panels } = this.props
    let activePanel = panels[this.state.activeStep]
    console.log("ACTIVEPANEL: ", activePanel)
    return (
    	<div>
				<div style={{ ...styles.wbt_background, ...this.props.style[this.state.activeStep]}}>
					<div class="container">
						<div style={styles.panel}>
							<CSSTransitionGroup
			          transitionName={this.state.next ? "carouselPanel_NEXT" : "carouselPanel_BACK"}
			          transitionEnterTimeout={800}
			          transitionLeaveTimeout={800}>
									<div key={`panel_${this.state.activeStep}`} style={styles.panelHolder}>
										<Section children={Array.isArray(activePanel) ? activePanel : [ activePanel ]}  hideTitle={true} sectionId={'carousel'} title={`carouselPanel_${this.state.activeStep}`} />
									</div>
								</CSSTransitionGroup>
						</div>
		      </div>
	      </div>
	      <div class="container" >
		      <MobileStepper
				      	style={styles.nav}
				        variant="progress"
				        steps={panels.length}
				        position="static"
				        activeStep={this.state.activeStep}
				        className={classes.root}
				        nextButton={
				          <Button size="small" onClick={this.handleNext} disabled={this.state.activeStep === panels.length - 1}>
				            Next
				            {theme.direction === 'rtl' ? <Icon class="material-icons">chevron_left</Icon> : <Icon class="material-icons">chevron_right</Icon>}
				          </Button>
				        }
				        backButton={
				          <Button size="small" onClick={this.handleBack} disabled={this.state.activeStep === 0}>
				            {theme.direction === 'rtl' ? <Icon class="material-icons">chevron_right</Icon> : <Icon class="material-icons">chevron_left</Icon>}
				            Back
				          </Button>
				        }
				      />
	      </div>
      </div>
    )
  }
}

Carousel.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
}

export default withStyles(styles, { withTheme: true })(Carousel)