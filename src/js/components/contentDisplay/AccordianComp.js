import React from 'react'

import ReactHtmlParser from "react-html-parser"

import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'
import ExpansionPanel, { ExpansionPanelDetails, ExpansionPanelSummary } from 'material-ui/ExpansionPanel'
import Button from 'material-ui/Button'
import Typography from 'material-ui/Typography'
import Icon from 'material-ui/Icon';

const styles = theme => ({
    root: {
        flexGrow: 1,
        paddingBottom: '5%',
    },
    heading: {
        fontSize: 25,
        fontWeight: 100
    }
})

class AccordianComp extends React.Component {
    state = {
        expanded: null,
        completed: {}
    }

    handleChange = panel => (event, expanded) => {
        this.setState({ expanded: expanded ? panel : false })
    }
	
  render() {
    const { classes, panels, style } = this.props
    const { expanded } = this.state

    return (
    	<div class="container" style={ style }>
			<div className={ classes.root }>
                { _.map(panels, (panel, index) => {
                    return (
                        <ExpansionPanel key={ `panel_${index}` } expanded={ expanded === `panel_${index}` } onChange={ this.handleChange(`panel_${index}`) }>
                            <ExpansionPanelSummary expandIcon={ <Icon>expand_more</Icon> }>
                                <Typography variant='subheading' className={ classes.heading }>{ ReactHtmlParser(panel.label) }</Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails>
                                { ReactHtmlParser(panel.content) }
                            </ExpansionPanelDetails>
                        </ExpansionPanel>
                    )
                }) }
            </div>
        </div>
    )
  }
}

AccordianComp.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles, { withTheme: true })(AccordianComp)