import React from 'react'

import ReactHtmlParser from "react-html-parser"

import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'
import Card, { CardActions, CardContent } from 'material-ui/Card'
import Button from 'material-ui/Button'
import Typography from 'material-ui/Typography'
import Icon from 'material-ui/Icon';

const styles = theme => ({
    card: {
        minWidth: 275,
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    CardContent: {
        position: 'relative',
    },
    CardActions: {
        marginTop: 'auto',
    },
    heading: {
        fontSize: 25,
        fontWeight: 100
    }
})

class CardComp extends React.Component {
	
    render() {
        const { classes, data, style } = this.props

        return (
        	<Card class={ classes.card } style={ data.style }>
                <CardContent class={ classes.CardContent }>
                    <Typography style={ {color: data.style.color} } variant="headline" component="div">{ ReactHtmlParser(data.title) }</Typography>
                    <Typography style={ {color: data.style.color} } component="div">{ ReactHtmlParser(data.text) }</Typography>
                </CardContent>
                <CardActions class={ classes.CardActions }>
                    <Button style={ {color: data.style.color} } size="small">Download PDF?</Button>
                </CardActions>
            </Card>
        )
    }
}

CardComp.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles, { withTheme: true })(CardComp)