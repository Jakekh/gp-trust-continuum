var React = require('react')
var $ = require('jquery')
import _ from 'lodash'
import ReactDOM from 'react-dom'

import Button from 'material-ui/Button'
import Icon from 'material-ui/Icon'
import IconButton from 'material-ui/IconButton'
import Paper from 'material-ui/Paper'
import ReactHtmlParser from "react-html-parser"
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'
import GridList, { GridListTile, GridListTileBar  } from 'material-ui/GridList'

import * as CourseActions from '../../actions/CourseActions'
import LmsStore from '../../stores/LmsStore'
import MsgStore from '../../stores/MsgStore'
import ModalStore from '../../stores/ModalStore'



export default class ClickandDiscover extends React.Component {
	constructor(props){
		super(props)
		const savedState = LmsStore.returnInteractionData(props.componentId)
		if(savedState != undefined){
			this.state = {
				selectedItemIndex: savedState.si,
				selectedItem: props.pairs[savedState.si],
				completed: savedState.c,
				svg: props.type == 'svg' ? <object id="svg-items" type="image/svg+xml" data={ this.props.svgUrl } /> : null
			}
		}else{
			this.state = {
				complete: false,
				completed: Array(props.pairs.length).fill(0),
				svg: props.type == 'svg' ? <object id="svg-items" type="image/svg+xml" data={ this.props.svgUrl } /> : null
			}
		}
	}

	componentDidMount() {
		const that = this
		const { pairs, type } = this.props
		switch(type){
			case 'svg' :
				_.map(pairs, (value, key) => {
					const svg = document.getElementById('svg-items')
					svg.addEventListener("load", () => {
						const svgDoc = svg.contentDocument
						const svgItem = svgDoc.getElementById(value.item)
						const checkInd = svgDoc.getElementById(`check_${key}`)
						$(svgItem).css('cursor', 'pointer')
						$(svgItem).on('click', () => {
							$(checkInd).css('display', 'block')
							that.selectItem(key)
						})
						this.state.completed[key] == 1 ? $(checkInd).css('display', 'block') : null
					})
				})
			break
			case 'launchable' :

			break
			case 'terms' : 

			break
		}
		
	}

	selectItem = (index) => {
		const that = this
		const item = this.props.pairs[index]
		const outId = this.state.selectedItem ? this.state.selectedItem.item.replace(/\W+/g, '-').toLowerCase() : null
		const defId = item.item.replace(/\W+/g, '-').toLowerCase() 
		$(`#${outId}`).removeClass('expanded').addClass('collapsed')

		let newAry = this.state.completed
		newAry[index] = 1
		
		this.setState({
			selectedItemIndex: index,
			selectedItem: item,
			completed: newAry
		}, function(){
			switch(this.props.type){
				case "svg" :
				case "terms" :
				case "statement" :
					_.map(that.props.pairs, (value, key) => { $(`#statement_part_${key}`).removeClass('highlight') })
					$(`#statement_part_${index}`).addClass('highlight')
				case "images-full" :
				case "images" :
					$(`#${defId}`).removeClass('expanded').addClass('collapsed')
					setTimeout(function(){
						$(`#${defId}`).removeClass('collapsed').addClass('expanded')
					}, 200)
				break
				case "launchable" :
					window.open(  `${ item.url }` , '_blank')
				break

			}
			CourseActions.saveInteraction({id: that.props.componentId, si: that.state.selectedItemIndex, c: that.state.completed})
			this.checkComplete()
		})
	}

  	checkComplete = () => {
		const { completed } = this.state
		const { pairs } = this.props
		let complete = true
		_.map(pairs, (value, key) => {
			completed[key] == 0 ? complete = false : null
		})
		complete && this.setComplete()
  	}

  	setComplete = () => {
	  	const { getNextThreshold } = this.props
	  	!this.state.complete && getNextThreshold && getNextThreshold()
	  	this.setState({ complete: true })
  	}

	closeDef = () => {
		const that = this
		const defId = this.state.selectedItem.item.replace(/\W+/g, '-').toLowerCase() 
		$(`#${defId}`).removeClass('expanded').addClass('collapsed')
		setTimeout(function(){
			that.setState({
				selectedItem: null
			}, function(){
				CourseActions.saveInteraction({id: that.props.componentId, si: that.state.selectedItem, c: that.state.completed})
			})
		}, 1000)
	}
	render() {
		const{ title, directions, pairs, type, componentId, icon, style } = this.props
		const { selectedItem, completed, svg } = this.state
		const styles = {
			centeredContent: {
				width: "100%",
				textAlign: "center",
			},
			button: {
				margin: "20px 10px",
			},
			interactionCont:{
				transition: "all 500ms ease-in",
			},
			shadow: {
			    boxShadow: "0 0 100px rgba(128, 128, 128, 0.3) inset",
			    color: "#898989",
			    background: "rgb(236, 236, 236)",
			},
			inner: {
				minWidth: "100%",
			    color: "#898989",
		        margin: '0 auto',
			    boxShadow: "0 0 100px rgba(128, 128, 128, 0.3) inset",
			    background: "rgb(236, 236, 236)",
			    marginTop: 60,
			},
			interactiveIcon: {
				fontSize: "80px",
				color: "#898989",
			},
			defBox: {
				position: "relative",
				maxWidth: "1200px",
				textAlign:"left",
				padding: "20px",
				margin: "auto",
				background: "#fff",
				border: '1px solid #6c276a',
				display: "inline-block"
			},
			svgBoxCont: {
				padding: "0 5% 0 0",
			},
			svgDefBox: {
				position: "relative",
				maxWidth: "1200px",
				textAlign:"left",
				padding: "20px",
				margin: "auto",
				background: "#fff",
				marginTop: "20%",
			},
			closeDefIcon: {
				position: "absolute",
				right: "10px",
				top: "10px",
			},
			defTitle: {
				marginTop: "0px",
			},
			root: {
				display: 'inline-block',
			},
			gridList: {
				display: 'flex',
				flexWrap: 'nowrap',
				overflowX: 'auto',
			},
			titleStyle: {
				color: 'rgb(0, 188, 212)',
			},
			respImg: {
				cursor: "pointer",
				maxWidth: "100%",
				width: "auto",
			},
			icon: {
				color: "#fff",
			},
			title: {
				fontWeight: "bold",
				textShadow: "none",
			}
		}

		const that = this
		let children = []
		switch(type){
			case 'terms' :
			case 'statement' :
				children = _.map(pairs, (value, key) => {
					return <Button color='primary' key={key} variant="raised" onTouchTap={that.selectItem.bind(that, key)} style={styles.button}>{completed[key] == 1 ? <Icon>check_circle</Icon> : null}&nbsp;&nbsp;{value.item}</Button>
				})
			break
			case 'images' :
				children = _.map(pairs, (value, key) => {
					return <GridListTile
					          key={key}
					          onTouchTap={that.selectItem.bind(that, key)}
					        >
					            <img src={value.item} />
					            <GridListTileBar 
					            	style={styles.title}
					        		title={value.title}
					          		actionIcon={completed[key] == 1 ? <Icon style={styles.icon}>check_circle</Icon> : null} > 
				          		</GridListTileBar>
					        </GridListTile>
				})
			break
			case 'images-full' :
				children = _.map(pairs, (value, key) => {
					return <div style={{padding: 20, display: "inline-block"}}><img onTouchTap={that.selectItem.bind(that, key)} key={key} src={value.item} style={styles.respImg} /></div>
				})
			break
			case 'launchable':
				children = _.map(pairs, (value, key) => {
					return <Button key={key} onTouchTap={that.selectItem.bind(that, key)} primary={true} style={styles.button} icon={completed[key] == 1 ? <Icon>check_circle</Icon> : null}>{value.title}</Button>
				})
			break
			case 'svg':

			break
		}

		const defId = selectedItem ? selectedItem.item.replace(/\W+/g, '-').toLowerCase() : null
		const defBox = selectedItem && type != "launchable" ? <div id={defId}>
										<Paper style={type == 'svg' ? styles.svgDefBox : styles.defBox} key={selectedItem.item}>
											<IconButton onTouchTap={this.closeDef} style={styles.closeDefIcon}>
									     		<Icon className="material-icons" >close</Icon>
										    </IconButton>
											<h3 style={styles.defTitle}>{type == 'svg' || type == 'images' || type == 'images-full' ? selectedItem.title : selectedItem.item}</h3>
											<div style={styles.defBoxContent}>
												{ ReactHtmlParser(selectedItem.def) }
											</div>
										</Paper>
									</div> : null 

		let gridClass = "container mdl-grid"
		!this.props.columnSpacing ? gridClass += " mdl-grid--no-spacing" : null

		return (
			<div style={ title ? this.props.innerChild ? { ...styles.inner, ...style } : { ...styles.shadow, ...style } : style }>
				<div style={ styles.interactionCont } class={ gridClass } key={ componentId }>
					<div style={ styles.centeredContent }>
						{ icon ? <div class="golfBall"><Icon className="material-icons" >{icon}</Icon></div> : null }
						{ title ? <h2 >{title}</h2> : null }
						{ directions ? <h4 >{directions}</h4> : null }
						{ type == 'terms' || type == 'statement' ? 
							<div>
								{ children }
								{ defBox }
							</div>
						: 
							<div>
								{ type == 'svg' ? 
									<div class={gridClass}>
										<div class="mdl-cell mdl-cell--6-col" style={styles.svgBoxCont}>{ svg }</div>
										<div class="mdl-cell mdl-cell--6-col">{ defBox }</div>
									</div>
								: null }
								{ type == "images" ? 
									<div style={styles.root}>
    									<GridList style={styles.gridList} cellHeight={360} cols={5}>
    										{ children }
    									</GridList>
    									{ defBox ? defBox : <div>&nbsp;</div> }
  									</div>
								: null }
								{ type == "images-full" ? 
									this.props.sideBySide ? 
										<div class="mdl-grid">
											<div class="mdl-cell mdl-cell--6-col">
												{ children }
											</div>
											<div class="mdl-cell mdl-cell--6-col">
												{ defBox ? defBox : <div>&nbsp;</div> }
											</div>
										</div>
									:
										<div style={styles.root}>
											{ children } { defBox ? defBox : <div>&nbsp;</div> }
										</div>
    									
								: null }
								
							</div>
						}
					</div>
				</div>
			</div>
		)	
	}
}
