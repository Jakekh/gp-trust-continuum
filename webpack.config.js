// to set the NODE_ENV to production use -- set NODE_ENV=production then run webpack
// this minifies/uglifis the script

var debug = false;
var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var path = require("path")

module.exports = {
    context: __dirname,
    devtool: debug ? "inline-sourcemap" : false,
    entry: {
        app: __dirname +"/src/js/scripts.js",
        vendor: ["babel-polyfill", "jquery", "lodash", "react", "material-design-lite", "material-ui", "react-html-parser", "react-dom"]
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015', 'stage-0'],
                    plugins: ['react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy'],
                }
            },
            {
                test: /\.node$/,
                loader: "node-loader"
            },
            {
                test: /\.json$/,
                loader: "json-loader"
            },
            { 
                test: /\.css$/, 
                loader: "style-loader!css-loader" 
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2|otf)$/,
                loader: 'url-loader?limit=100000'
            }
        ]
    },
    node: {
        fs: 'empty',
        net: 'empty',
        tls: 'empty',
        dns: 'mock',
        net: 'mock'
    },
    resolve: {
        extensions: [".webpack.js", ".web.js", ".js", ".node"],
        alias: {
          "TweenLite": path.resolve('node_modules', 'gsap/src/uncompressed/TweenLite.js'),
          "TweenMax": path.resolve('node_modules', 'gsap/src/uncompressed/TweenMax.js'),
          "TimelineLite": path.resolve('node_modules', 'gsap/src/uncompressed/TimelineLite.js'),
          "TimelineMax": path.resolve('node_modules', 'gsap/src/uncompressed/TimelineMax.js'),
          "TextPlugin": path.resolve('node_modules', 'gsap/src/uncompressed/plugins/TextPlugin.js'),

          "ScrollMagic": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js'),
          "animation.gsap": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js'),
          "debug.addIndicators": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js')
        }
    },
    output: {
        path: __dirname+"/build",
        filename: "courseware.js"
    },
    plugins: debug ? [
        new webpack.optimize.CommonsChunkPlugin({name: "vendor", filename: "vendor.bundle.js"}),
        /*new CopyWebpackPlugin([
             { from: 'src/data/course-assets', to: 'src/data/course-assets' },
             { from: 'index.html' }
        ])*/
    ] : [
        new webpack.DefinePlugin({
            'process.env': {
              'NODE_ENV': JSON.stringify('production')
            }
          }),
        new webpack.optimize.CommonsChunkPlugin({name: "vendor", filename: "vendor.bundle.js"}),
        new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
        new CopyWebpackPlugin([
             { from: 'src/data/course-assets', to: 'src/data/course-assets' },
             { from: 'index.html' }
        ])
    ],
};